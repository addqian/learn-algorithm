# INTRO

simple code for algorithm learning with java/python/c

# SET

Project Settings: Modules

- [Python] Dependencies: Module SDK: Python 3.9.18
- [Java] Sources: Language Level: 11

- /src/main
- /src/test

# Structure

    learn-algorithm
        ├─  .idea
        └─  src
            ├─  main
            │   ├─  common_algorithm
            │   ├─  common_code
            │   ├─  common_hadle for resouces
            │   ├─  common_unit for tool
            │   ├─  db_connect
            │   ├─  decorator for @method
            │   ├─  dynamic_planning
            │   ├─  process_control
            │   └─  ..
            └─  test

# Env

## Python

python -V
Python 3.11.8+
python -m venv .venv
[Windows] cd .venv && cd Scripts  && activate && cd ../..  && cls
python -m pip install --upgrade pip
python install -r requirements.txt

### Dependence

    pip install django djangorestframework