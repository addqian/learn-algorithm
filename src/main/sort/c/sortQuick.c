

#include <stdio.h>

void quickSort(int ar[], const int left, const int right) {
    if (left < right) {
        int l = left;
        int r = right;
        int idx = ar[(l + r) / 2];

        while (l <= r) {
            while (ar[l] < idx) ++l;
            while (ar[r] > idx) --r;

            if (l <= r) {
                int temp = ar[l];
                ar[l] = ar[r];
                ar[r] = temp;
                ++l;
                --r;
            }
        }
        quickSort(ar, left, r);
        quickSort(ar, l, right);
    }
}

int main() {
    int ar[] = {10, 7, 8, 9, 1, 5};
    int n = sizeof(ar) / sizeof(ar[0]);

    quickSort(ar, 0, n - 1);
    for (int i = 0; i < n; ++i) {
        printf("%d ", ar[i]);
    }
    return 0;
}
