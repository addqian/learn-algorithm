package main.sort.java;

public class SortShell extends Swap implements Sort {
    @Override
    public void execute(int[] ar) {
        sortShell(ar, ar.length);
    }

    private void sortShell(int[] ar, int len) {
        for (int step = len >> 1; step > 0; step >>= 1) {
            for (int i = step; i < len; ++i) {
                int j = i, cursor = ar[j];
                while (j >= step && ar[j - step] > cursor) {
                    Swap.execute(ar, j, j - step);
                    j -= step;
                }
                if (j != i) {ar[j] = cursor;}
            }
        }
    }
}