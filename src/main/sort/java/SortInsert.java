package main.sort.java;

public class SortInsert extends Swap implements Sort {

    @Override
    public void execute(int[] ar) {
        sortInsert(ar, ar.length);
    }

    private void sortInsert(int[] ar, int len) {
        for (int i = 1; i < len; ++i) {
            int j = i;
            int cursor = ar[j];
            while (j >= 1 && ar[j - 1] > cursor) {
                ar[j] = ar[--j];
            }
            if (j != i) {ar[j] = cursor;}
        }
    }
}