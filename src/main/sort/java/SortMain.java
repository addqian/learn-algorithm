package main.sort.java;

import java.util.Arrays;


public class SortMain {
    public static void main(String[] args) {
        int[] ar = new int[]{1, 3, 5, 7, 9, 2, 4, 6, 8, 0};
        System.out.println(Arrays.toString(ar));

        /**
         * Sorting using strategy pattern
         */
        ContextSort contextSort = new ContextSort();

        // set different sort strategy
        //Arrays.sort(ar);
        contextSort.setSort(new SortBubble());
        //contextSort.setSort(new SortQuick());
        //contextSort.setSort(new SortSelect());
        //contextSort.setSort(new SortHeap());
        //contextSort.setSort(new SortInsert());
        //contextSort.setSort(new SortShell());
        //contextSort.setSort(new SortMerge());
        contextSort.execute(ar);

        System.out.println(Arrays.toString(ar));
    }
}