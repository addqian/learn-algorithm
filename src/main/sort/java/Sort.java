package main.sort.java;

interface Sort {
    void execute(int[] ar);
}