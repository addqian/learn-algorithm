package main.sort.java;

public class SortBubble extends Swap implements Sort {
    @Override
    public void execute(int[] ar) {
        sortBubble(ar, ar.length);
    }

    private void sortBubble(int[] ar, int len) {
        for (int i = 0; i < len - 1; i++) {
            for (int j = 0; j < len - i - 1; j++) {
                if (ar[j] > ar[j + 1]) {
                    Swap.execute(ar, j, j + 1);
                }
            }
        }
    }
}