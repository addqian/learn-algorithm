package main.sort.java;

public class ContextSort {
    private Sort sort;

    public void execute(int[] ar) {
        sort.execute(ar);
    }

    public void setSort(Sort sort) {this.sort = sort;}
}
