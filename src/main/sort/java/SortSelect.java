package main.sort.java;

public class SortSelect extends Swap implements Sort {
    @Override
    public void execute(int[] ar) {
        sortSelect(ar, ar.length);
    }

    private void sortSelect(int[] ar, int len) {
        for (int i = 0; i < len - 1; ++i) {
            int index = i;
            for (int j = i + 1; j < len; ++j) {
                if (ar[j] < ar[index]) {
                    index = j;
                }
            }
            if (index != i) {Swap.execute(ar, i, index);}
        }
    }
}
