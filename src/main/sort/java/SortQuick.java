package main.sort.java;

public class SortQuick extends Swap implements Sort {
    @Override
    public void execute(int[] ar) {
        sortQuick(ar, 0, ar.length - 1);
    }

    private void sortQuick(int[] ar, int left, int right) {
        if (left >= right) {return;}

        int l = left, r = right;
        int i = l, flag = ar[i];

        while (l < r) {
            while (l < r && flag <= ar[r]) {--r;}
            if (l < r) {ar[l++] = ar[r];}
            while (l < r && ar[l] <= flag) {++l;}
            if (l < r) {ar[r--] = ar[l];}
        }
        if (l == r) {ar[l] = flag;}
        if (left < l - 1) {sortQuick(ar, left, l - 1);}
        if (r + 1 < right) {sortQuick(ar, r + 1, right);}
    }
}
