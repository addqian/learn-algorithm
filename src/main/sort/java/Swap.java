package main.sort.java;

abstract class Swap {
    public static void execute(int[] ar, int i, int j) {
        if (i != j && ar[i] != ar[j]) {
            ar[i] ^= ar[j];
            ar[j] ^= ar[i];
            ar[i] ^= ar[j];
        }
    }
}