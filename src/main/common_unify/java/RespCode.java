package common_unify.java;

import lombok.Getter;

@Getter
public enum RespCode {
    SUCCESS(200, "Success"),
    CLIENT_ERROR(400, "Client Error"),
    PATH_NOT_FOUND(404, "Path Not Found"),
    SERVER_ERROR(500, "Server Error"),
    PARAM_ERROR(501, "Param Error"),;

    /**
     * 状态码
     */
    private final int code;
    /**
     * 状态
     */
    private final String state;

    RespCode(int code, String state) {
        this.code = code;
        this.state = state;
    }
}
