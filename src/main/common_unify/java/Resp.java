package common_unify.java;

import lombok.Data;

@Data
public class Resp<T> {
    private int code;
    private String msg;
    private T content;
    // 时间戳
    private long timestamp;

    private Resp() {this.timestamp = System.currentTimeMillis();}

    private Resp(int code, String msg) {
        this();
        this.code = code;
        this.msg = msg;
    }

    private Resp(T content) {
        this(RespCode.SUCCESS.getCode(), RespCode.SUCCESS.getState());
        this.content = content;
    }

    public static <T> Resp<T> ok(T... content) {
        System.out.println(content.length);
        //if (content.length == 0) {
        //    return new Resp<>();
        //}
        //return new Resp<T>();
        return null;
    }

    public static <T> Resp<T> fail(int code, String msg) {return new Resp<>(code, msg);}
}