"""
字符串通配符_牛客题霸_牛客网
https://www.nowcoder.com/practice/43072d50a6eb44d2a6c816a283b02036

[I]
te?t*.*
txt12.xls
[O]
false
[I]
**Z
0QZz
[O]
true
[I]
h*h*ah**ha*h**h***hha
hhhhhhhahhaahhahhhhaaahhahhahaaahhahahhhahhhahaaahaah
[O]
false
"""

import re


def main():
    p = input().lower()
    s = input().lower()

    p = p.replace('.', '\\.')
    p = p.replace('?', '[a-zA-Z0-9]')
    p = re.sub('\\*+', '[a-zA-Z0-9]*', p)

    p = re.compile(p)
    if p.fullmatch(s):
        print("true")
    else:
        print("false")


if __name__ == '__main__':
    main()
