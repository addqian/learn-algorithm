"""
字符串最后一个单词的长度_牛客题霸_牛客网
https://www.nowcoder.com/practice/8c949ea5f36f422594b306a2300315da

"""


def handle2(row: str):
    return len(row[row.rindex(' ') + 1 if ' ' in row else 0:])


def handle(row: str):
    return len(row.split(' ')[-1])


def main():
    row = input()
    res = handle2(row)
    print(res)


if __name__ == '__main__':
    main()
