"""
矩阵乘法_牛客题霸_牛客网
https://www.nowcoder.com/practice/ebe941260f8c4210aa8c17e99cbc663b

[I]
2
3
2
1 2 3
3 2 1
1 2
2 1
3 3
[O]
14 13
10 11
"""


def main():
    x = int(input())
    y = int(input())
    z = int(input())

    m1 = []
    for i in range(x):
        m1.append(list(map(int, input().split())))

    m2 = []
    for i in range(y):
        m2.append(list(map(int, input().split())))

    m = [[0] * z for _ in range(x)]
    for i in range(x):
        for j in range(z):
            m[i][j] = 0
            for k in range(y):
                m[i][j] += m1[i][k] * m2[k][j]

    for e in m:
        print(*e)


if __name__ == '__main__':
    main()
