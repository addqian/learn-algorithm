"""
密码截取_牛客题霸_牛客网
https://www.nowcoder.com/practice/3cd4621963e8454594f00199f4536bb1
知识点 字符串 动态规划


用例输入
LONNNNP
预期输出
4

输入：
emrkuxrcwzmempvwzxemfdviljbooqktqqvpenpkzzmjnesmsuutokofmmdlyjzcwfemfkwloinozvrlusecejzzhbporlzosrqvfjtsztydlsmhcjeyexjrqctkndskvrqeoodfwjscjcngwpqwhknoqliglepwictufzhgmkqhlyvhljhfnmfvvrpthjcrbxeuwxylxoqvftbrtotocsyrxjvcdmfypzqzpqoypeslessesowvklltdvxsuuvjlfqlbvtujgyzpxkkdmquxexrdmukromgkoyzisxcjurnpohnncswiydiprqnndwnqrpvwypzhdmuopmyjcrvxujkrzpnfptgwqhsvcdxvzogwgknkrevkildfhsckndpfnbgdxsqtdzjzwgnjeclkyxjcgvimnpssizopnpjjjpuzxfwzoiwjmdxerdfqlwodtdonewrrtwwndtcqmbgxqyqfjbmokkvxetqbesqppbbepiuhbseuogbhihboyqlxlxyeyecukwmwgotkcnderzxouyvydbwyidsbwexngztcymfhqttmmrtujopqcfsnyskopqzdubccbtcibppdqtuorxzjrqblsgplrknwmqxvfhiplsbqustqbpdbuewcttbuzujhmcgqeglcmkdldrmpzvulsvwzehjbkxcndlbeekurgimxzczzhfcqhlzxzqmrdsdybnycphywhxczzuhypynqlhdeydnbuxyxotkktigrpnvubfvwzhjpgsshthrxdpyzclubmqyfhiueznzkmozxbxgqhwsvdmkqklotyzxyjvgxxmelzwoobdztvczdovferihxbckzvgfbtltroqwvrizwtylpoewmnsxfqcdgddffqlvcyrnzbphiptiwvvvnyurxhdjmqwoetzueztgtlkonhgswyxeqjqosjxyxhjouzbydncfqsicmqirbmwtqbzrwjlvqupyinbgggknjxwdnlukoefogwngwnpreslfyjndrljshxiizfmiuxvqyekfxekokrffkmpekjhvqjpivqjxpbtwrhnxpuyryzhnoysvrrlvyvttolecrvgqgodjzffsgfekdhclrplqffditgrkpigkucgnesisqlqokrqkztexrletocwwnyokuhwqklskylmscmmdkjjnnikrwxvxcyisqilqhoccoetvicedelcyerusflxhzpuddeehzoulxcyriqvivlivqkcfyllrnyhdlocfufyojnzldcwtbtdbqtguwjwloutdefqfulyyxdmkipzxcuimukutvxbfwpkosyrydnwbyoczjxjpnnrnyfpmijqcqirkdwhgbnfkecfdrdixcsdtyxlbuwkckhhqvrwiqfuhvbpnepyituukkrufmxzroktjrmpxjqobmviwqecehvpclwrdfkfdrffwirgodjnysrkpgdyysivbbzogyszulhozhjzrvklcnivrogyjqznbzxwwuyffvjwsrciukdywkygigwyvyeybfgetxexcgrgfzbnsrjxkkskojzpgmcmorywypydnkrvsgkhwqzemnnzwhgzisufmhnygxhnkvjjeoxdiqesoolzckogygcrnohdbqknotcwmlyshqffrlqxxylezxtszsejvtpfvcqxuffheecyocmkiuxtrrbxpnynistfyielxgbevumyhwirsbtrzdxzvfoxdrmsdhizhzuvdrcztmtyxmpfezgejptqpinpbgvwcgyogfulmjxupwlxdjrzutnsnmjfcnumczhczpwxvgkdgbpfvluhrdqzhinrquygtkdgkxzhgtqhttlkcqtbigktsfjionmbvfivyxppxzpcqvjvziirhobsmxicuyqbfpqlkkbprlnpbfctvcszcdohkjehugdypqoghpdsenotktsfkxjiyovygwgjslnekjujcvrvzrflfjxjfdflqdzrhuubfyzbglmgjhzsy
输出：
7


输入：
ABBBA
输出：
5

输入：
12HHHHA
输出：
4

"""


def handle_odd_even(s: str):  # ⭐

    def max_sub(s):
        max_len = 1
        slen = len(s)

        for i in range(slen):
            max_len = max(
                max_len,
                max_sub_odd(s, i, slen),
                max_sub_even(s, i, slen)
            )

        return max_len

    def max_sub_odd(s: str, i: int, slen: int):  # 奇数
        max_len = 1

        for j in range(1, min(i, slen - i - 1) + 1):
            if s[i - j] == s[i + j]:
                max_len += 2
            else:
                return max_len
        return max_len

    def max_sub_even(s: str, i: int, slen: int):  # 偶数
        max_len = 0

        for j in range(0, slen - i - 1):
            if s[i - j] == s[i + j + 1]:
                max_len += 2
            else:
                return max_len
        return max_len

    print(max_sub(s))


def handle(s: str, slen: int):
    max_len = 0

    for i in range(slen, 1, -1):
        for j in range(i, max_len, -1):
            if s[i-j] == s[i-1] and s[i - j:i] == s[i - j:i][::-1]:
                max_len = j
                break

    print(max_len)


def handle_loop(s: str, slen: int):
    # run timeout

    max_len = 1

    for i in range(slen):
        j = max_len
        while i+j < slen:
            j += 1
            t = s[i:i+j]
            if t[0] == t[-1] and t == t[::-1]:
                max_len = j

    print(max_len)


def handle_loop2(s: str, slen: int):
    # run timeout

    max_len = 1

    for i in range(slen):
        j = max_len
        while i+j < slen:
            j += 1
            if s[i] == s[i+j-1]:
                t = s[i:i+j]
                if t == t[::-1]:
                    max_len = j

    print(max_len)


def main():
    s = input()
    slen = len(s)
    handle_loop(s, slen)
    # handle(s, slen)


if __name__ == '__main__':
    main()
