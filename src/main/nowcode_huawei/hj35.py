"""
"""


def main():
    n = int(input())
    row = 1
    row_diff = 1
    for i in range(n):
        row += i
        row_diff += 1
        print(row, end="")
        col = row
        col_diff = row_diff
        for j in range(1, n - i):
            col += col_diff
            print("", col, end="")
            col_diff += 1
        print()


def main2():
    num = int(input())
    row = []
    for i in range(num):
        if i == 0:
            row = [(x + 2) * (x + 1) // 2 for x in range(num)]
        else:
            row = [x - 1 for x in row[1:]]
        print(' '.join(map(str, row)))


if __name__ == "__main__":
    main()
