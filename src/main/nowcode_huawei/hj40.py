"""
https://www.nowcoder.com/practice/539054b4c33b4776bc350155f7abd8f5
HJ40 统计字符
"""
from __init__ import redirect_input

redirect_input(__file__)


def main():
    s = input()
    alpha_num = len([e for e in s if e.isalpha()])
    print(alpha_num)
    space_num = len([e for e in s if e == ' '])
    print(space_num)
    digit_num = len([e for e in s if e.isdigit()])
    print(digit_num)
    other_num = len(s) - alpha_num - digit_num - space_num
    print(other_num)


def main1():
    alpha_num, space_num, digit_num, other_num = 0, 0, 0, 0
    s = input()
    for e in s:
        if e.isalpha():
            alpha_num += 1
        elif e.isdigit():
            digit_num += 1
        elif e == ' ':
            space_num += 1
        else:
            other_num += 1
    print(alpha_num)
    print(space_num)
    print(digit_num)
    print(other_num)


if __name__ == '__main__':
    """
    26
    3
    10
    12
    """
    main()
