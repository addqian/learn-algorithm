"""
字符串排序_牛客题霸_牛客网
https://www.nowcoder.com/practice/5190a1db6f4f4ddb92fd9c365c944584
"""


def main():
    s = list(input())
    s_len = len(s)

    s_alpha = [e for e in s if e.isalpha()]
    s_alpha_sort = sorted(s_alpha, key=lambda x: ord(x.upper()))

    for i in range(s_len - 1, -1, -1):
        if s[i].isalpha():
            s[i] = s_alpha_sort.pop()

    print(''.join(s))
    pass


def main2():
    row = input()

    row_alpha_order = sorted([c for c in row if c.isalpha()], key=lambda x: x.lower())
    for i in range(len(row)):
        if not row[i].isalpha():
            row_alpha_order.insert(i, row[i])

    print(''.join(row_alpha_order))


if __name__ == '__main__':
    main()
