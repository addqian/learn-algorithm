"""
迷宫问题_牛客题霸_牛客网
https://www.nowcoder.com/practice/cf24906056f4488c9ddb132f317e03bc


5 5
0 1 0 0 0
0 1 1 1 0
0 0 0 0 0
0 1 1 1 0
0 0 0 1 0
---
(0,0)
(1,0)
(2,0)
(2,1)
(2,2)
(2,3)
(2,4)
(3,4)
(4,4)


"""


def handle(row: int, col: int, maze: list[list]):
    def iterate_maze(path: list):
        i, j = path[-1]

        if (row - 1, col - 1) in path:
            print(*[f'({e[0]},{e[1]})' for e in path], sep='\n')

        if i + 1 < row and maze[i + 1][j] == 0 and (i + 1, j) not in path and (
                row - 1, col - 1) not in path:
            iterate_maze(path + [(i + 1, j)])
        if j + 1 < col and maze[i][j + 1] == 0 and (i, j + 1) not in path and (
                row - 1, col - 1) not in path:
            iterate_maze(path + [(i, j + 1)])
        if j - 1 >= 0 and maze[i][j - 1] == 0 and (i, j - 1) not in path and (
                row - 1, col - 1) not in path:
            iterate_maze(path + [(i, j - 1)])
        if i - 1 >= 0 and maze[i - 1][j] == 0 and (i - 1, j) not in path and (
                row - 1, col - 1) not in path:
            iterate_maze(path + [(i - 1, j)])

        if (row - 1, col - 1) not in path:
            path.pop()

    iterate_maze([(0, 0)])


def my(row, col, maze):
    def unfinished(steps: list[tuple]):
        return (steps[-1][0], steps[-1][1]) != (row - 1, col - 1)

    def handle(steps: list[tuple]):
        x, y = steps[-1][0], steps[-1][1]

        if (x, y) == (row - 1, col - 1):
            print(*[f'({e[0]},{e[1]})' for e in steps], sep='\n')
        if x + 1 < row and maze[x + 1][y] == 0 and (x + 1, y) not in steps and unfinished(
                steps):
            steps.append((x + 1, y))
            handle(steps)
        if y + 1 < col and maze[x][y + 1] == 0 and (x, y + 1) not in steps and unfinished(
                steps):
            steps.append((x, y + 1))
            handle(steps)
        if y - 1 >= 0 and maze[x][y - 1] == 0 and (x, y - 1) not in steps and unfinished(
                steps):
            steps.append((x, y - 1))
            handle(steps)
        if x - 1 >= 0 and maze[x - 1][y] == 0 and (x - 1, y) not in steps and unfinished(
                steps):
            steps.append((x - 1, y))
            handle(steps)

        if unfinished(steps):
            steps.pop()

    handle([(0, 0)])


def main():
    row, col = map(int, input().split())
    maze = [list(map(int, input().split())) for _ in range(row)]

    # handle(row, col, maze)
    my(row, col, maze)


if __name__ == '__main__':
    main()
