"""
24点运算_牛客题霸_牛客网
https://www.nowcoder.com/practice/7e124483271e4c979a82eb2956544f9d

[I]
K Q 6 K
[O]
NONE

[I]
K A 4 2
[O]
K+A*2-4
A+K*2-4
K/2*4*A
"""


def handle():
    """
    ✔️
    """
    values = ['/', 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

    def dfs(data: list[int], rlt, process: list[str]):
        if len(data) == 0:
            # print(process)
            if rlt == 24:
                return True, ''.join(process)
            return False, 'NONE'

        for i in range(len(data)):
            data = data[1:] + [data[0]]

            res = dfs(data[1:], rlt + data[0] if rlt else data[0],
                      process + [('+' if rlt else '') + values[data[0]]])
            if res[0] is True: return res
            res = dfs(data[1:], rlt - data[0] if rlt else data[0],
                      process + [('-' if rlt else '') + values[data[0]]])
            if res[0] is True: return res
            res = dfs(data[1:], rlt * data[0] if rlt else data[0],
                      process + [('*' if rlt else '') + values[data[0]]])
            if res[0] is True: return res
            res = dfs(data[1:], rlt / data[0] if rlt else data[0],
                      process + [('/' if rlt else '') + values[data[0]]])
            if res[0] is True: return res

        return False, 'NONE'

    def handle(s: str):
        if 'JOKER' in s:
            print('ERROR')
            return

        print(dfs([values.index(e) for e in s.split()], None, [])[1])

    s = input().upper()
    handle(s)


def handle2(cards):
    """
    ✔️
    """
    values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']
    order = range(1, 14)  # 1~13
    # {'A': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'J': 11, 'Q': 12, 'K': 13}
    card_order = dict(zip(values, order))

    def dfs(arr, target, current):
        if len(arr) == 1:
            if arr[0] == target:
                return values[arr[0] - 1] + current
            else:
                return None

        for i in range(len(arr)):
            new_arr = arr[:i] + arr[i + 1:]

            first = dfs(new_arr, target - arr[i], '+' + values[arr[i] - 1] + current)
            if first: return first
            second = dfs(new_arr, target + arr[i], '-' + values[arr[i] - 1] + current)
            if second: return second
            third = dfs(new_arr, target * arr[i], '/' + values[arr[i] - 1] + current)
            if third: return third
            fourth = dfs(new_arr, target / arr[i], '*' + values[arr[i] - 1] + current)
            if fourth: return fourth

    if 'joker' in cards or 'JOKER' in cards:
        print('ERROR')
        return
    vals = [card_order[x] for x in cards]

    ans = dfs(vals, 24, '')

    if ans:
        print(ans)
    else:
        print('NONE')


def handle3(cards):
    """
    ✔️
    """
    values = [None, 'A', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K']

    def calc_cards(res: int, cards_remain: list, formula: list):
        if len(cards_remain) == 0:
            if res == 24:
                print(''.join(formula))
                return True
            return False

        for i in range(len(cards_remain)):
            cards_remain = cards_remain[1:] + [cards_remain[0]]

            value = values.index(cards_remain[0])
            if calc_cards(res + value, cards_remain[1:], formula + ['+', cards_remain[0]]):
                return True
            if calc_cards(res - value, cards_remain[1:], formula + ['-', cards_remain[0]]):
                return True
            if calc_cards(res * value, cards_remain[1:], formula + ['*', cards_remain[0]]):
                return True
            if calc_cards(res // value, cards_remain[1:], formula + ['/', cards_remain[0]]):
                return True

        return False

    if 'joker' in cards or 'JOKER' in cards:
        print('ERROR')
        return

    for i in range(len(cards)):
        cards = cards[1:] + [cards[0]]
        if calc_cards(values.index(cards[0]), cards[1:], [cards[0]]): return

    print('NONE')


def main():
    cards = input().split()
    handle3(cards)


if __name__ == '__main__':
    main()
