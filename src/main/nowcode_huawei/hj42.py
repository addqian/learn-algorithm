"""
学英语_牛客题霸_牛客网
https://www.nowcoder.com/practice/1364723563ab43c99f3d38b5abef83bc
"""


def main():
    # and, billion, million, thousand, hundred
    def handle(num):
        s = ''
        if 1000000 <= num <= 2000000:
            s = f'{three_digit_value(num // 1000000)} million'
            num %= 1000000

        if 1000 <= num:
            s += f' {three_digit_value(num // 1000)} thousand'
            num %= 1000

        if 1 <= num:
            s += f' {three_digit_value(num)}'

        return s.strip()

    def three_digit_value(num):
        a = [
            'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten',
            'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen',
            'eighteen',
            'nineteen'
        ]
        b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

        s = ''
        if 100 <= num < 1000:
            s = f'{a[num // 100]} hundred'
            num %= 100
            s += f'{" and " if num != 0 else ""}'
        if 0 < num < 20:
            s += f'{a[num]}'
        if 20 <= num < 100:
            s += f'{b[num // 10]}{" " + a[num % 10] if num % 10 != 0 else ""}'
        return s

    # n = int(input())
    n = 49
    print(handle(n))


if __name__ == '__main__':
    main()
