"""
数据分类处理_牛客题霸_牛客网
https://www.nowcoder.com/practice/9a763ed59c7243bd8ab706b2da52b7fd
"""


def main():
    seq_i = input().split()[1:]
    seq_r = input().split()[1:]

    seq_r = sorted(set(list(map(int, seq_r))))
    seq_r = list(map(str, seq_r))

    d = {}
    count = 0
    for r in seq_r:
        d[r] = list()
        for i in range(len(seq_i)):
            if r in seq_i[i]:
                d[r].append((i, seq_i[i]))
                count += 2
        if len(d.get(r)) > 0:
            count += 2

    print(count, end='')
    for r in seq_r:
        if len(d[r]) > 0:
            print(f' {r} {len(d[r])}', end=' ')
            print(' '.join([f'{e[0]} {e[1]}' for e in d[r]]), end='')


if __name__ == '__main__':
    main()
