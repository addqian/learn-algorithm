"""
字符串合并处理_牛客题霸_牛客网
https://www.nowcoder.com/practice/d3d8e23870584782b3dd48f26cb39c8f
"""

from itertools import zip_longest


def main():
    def reverse_binary(s: str):
        c_d = {
            'a': '10',
            'b': '11',
            'c': '12',
            'd': '13',
            'e': '14',
            'f': '15',
        }
        d_c = {
            10: 'A',
            11: 'B',
            12: 'C',
            13: 'D',
            14: 'E',
            15: 'F',
        }

        s_new = ''
        for c in s:
            if c in '0123456789abcdefABCDEF':
                if c in 'ABCDEF':
                    c = c.lower()
                if c in 'abcdef':
                    c = c_d[c]

                c = int(f'{int(c):04b}'[::-1], 2)

                if c >= 10:
                    c = d_c[c]
            s_new += f'{c}'
        return s_new

    s = input().replace(' ', '')
    s0 = sorted(s[::2])
    s1 = sorted(s[1::2])

    s = ''.join([x for t in zip_longest(s0, s1, fillvalue='') for x in t if x])
    print(reverse_binary(s))


def main2():
    hex_num = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
               'a', 'b', 'c', 'd', 'e', 'f',
               'A', 'B', 'C', 'D', 'E', 'F']

    def helper(s):
        ten = int(s, 16)
        bc = format(ten, 'b').rjust(4, '0')
        bc = list(bc)
        bc.reverse()
        ten = int(''.join(bc), 2)
        hc = format(ten, 'x')
        return hc.upper()

    while True:
        try:
            a, b = input().strip().split()
            res = list(a + b)
            res[::2] = sorted(res[::2])
            res[1::2] = sorted(res[1::2])
            for i in range(len(res)):
                if res[i] in hex_num:
                    res[i] = helper(res[i])
            print(''.join(res))
        except EOFError:
            break


if __name__ == '__main__':
    main()
