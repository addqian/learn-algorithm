"""
Sudoku_牛客题霸_牛客网
https://www.nowcoder.com/practice/78a1a4ebe8a34c93aac006c44f6bf8a1

回溯

[I]
0 9 5 0 2 0 0 6 0
0 0 7 1 0 3 9 0 2
6 0 0 0 0 5 3 0 4
0 4 0 0 1 0 6 0 7
5 0 0 2 0 7 0 0 9
7 0 3 0 9 0 0 2 0
0 0 9 8 0 0 0 0 6
8 0 6 3 0 2 1 0 5
0 5 0 0 7 0 2 8 3


[I]
0 9 2 4 8 1 7 6 3
4 1 3 7 6 2 9 8 5
8 6 7 3 5 9 4 1 2
6 2 4 1 9 5 3 7 8
7 5 9 8 4 3 1 2 6
1 3 8 6 2 7 5 9 4
2 7 1 5 3 8 6 4 9
3 8 6 9 1 4 2 5 7
0 4 5 2 7 6 8 3 1
"""


def handle():
    def row_check(sudoku, i, j):
        if sudoku[i].count(sudoku[i][j]) > 1: return False
        return True

    def col_check(sudoku, i, j):
        if [sudoku[i][j] for i in range(ROW_NUM)].count(sudoku[i][j]) > 1: return False
        return True

    def area_check(sudoku, i, j):
        for _i in range(i // 3 * 3, i // 3 * 3 + 3):
            if sudoku[i][j] in [
                sudoku[_i][_j]
                for _j in range(j // 3 * 3, j // 3 * 3 + 3)
                if not (_i == i and _j == j)
            ]: return False
        return True

    def sudoku_check(sudoku, i, j):
        return (row_check(sudoku, i, j)
                and col_check(sudoku, i, j)
                and area_check(sudoku, i, j))

    def sudoku_fill(sudoku):
        for i in range(ROW_NUM):
            for j in range(COL_NUM):
                if sudoku[i][j] == 0:
                    for num in range(1, 10):
                        sudoku[i][j] = num
                        if sudoku_check(sudoku, i, j):
                            if sudoku_fill(sudoku):
                                return True
                        sudoku[i][j] = 0
                    return False
        return True

    ROW_NUM, COL_NUM = 9, 9
    sudoku = [list(map(int, input().split())) for _ in range(ROW_NUM)]

    # print(*sudoku, sep='\n')
    for s in sudoku:
        print(*s)


def deal_1():
    # ✔️
    def check(ar, x, y):
        for i in range(9):
            if i != x and ar[x][y] == ar[i][y]: return False
        for j in range(9):
            if j != y and ar[x][y] == ar[x][j]:  return False

        x0 = (x // 3) * 3
        y0 = (y // 3) * 3

        for i in range(x0, x0 + 3):
            for j in range(y0, y0 + 3):
                if (i != x or j != y) and ar[x][y] == ar[i][j]: return False
        return True

    def dsf(ar):
        for i in range(9):
            for j in range(9):
                if ar[i][j] == 0:
                    for e in range(1, 10):
                        ar[i][j] = e
                        if check(ar, i, j) and dsf(ar):
                            return True
                        else:
                            ar[i][j] = 0
                    return False
        return True

    ar = [[] for _ in range(9)]

    for i in range(9): ar[i] = list(map(int, input().split()))

    dsf(ar)

    for e in ar: print(*e)


def deal_2():
    # ✔️
    def filter_value(x, y):
        i, j = x // 3, y // 3
        grid = [m[i * 3 + r][j * 3 + c] for r in range(3) for c in range(3)]
        row_val = m[x]
        col_val = list(zip(*m))[y]
        return {1, 2, 3, 4, 5, 6, 7, 8, 9} - set(grid) - set(row_val) - set(col_val)

    def sudoku(pos):
        # ✔️
        if not pos:  # 填完了
            res.append([' '.join(map(str, i)) for i in m])
        else:
            x, y = pos[0]
            val = filter_value(x, y)
            if val:
                for v in val:
                    m[x][y] = v
                    sudoku(pos[1:])
                    m[x][y] = 0  # 回溯

    m = [list(map(int, input().split())) for i in range(9)]
    pos = [(x, y) for y in range(9) for x in range(9) if not m[x][y]]

    res = []
    sudoku(pos)
    print('\n'.join(res[0]))


def my(mtx: [[int]]):
    col_const, row_const = 9, 9

    def check(mtx: [[int]], r: int, c: int):
        d_row = [mtx[r][j] for j in range(9) if j != c]
        if mtx[r][c] in d_row: return False
        d_col = [mtx[i][c] for i in range(9) if i != r]
        if mtx[r][c] in d_col: return False
        d_box = [mtx[i][j] for i in range(r // 3 * 3, r // 3 * 3 + 3) for j in range(c // 3 * 3, c // 3 * 3 + 3) if
                 (i != r or j != c)]
        if mtx[r][c] in d_box: return False
        return True

    def contains(mtx: [[int]], r: int, c: int):
        s = set(range(1, 10))
        rows = [mtx[r][j] for j in range(9)]
        cols = [mtx[i][c] for i in range(9)]
        boxes = [mtx[i][j] for i in range(r // 3 * 3, r // 3 * 3 + 3) for j in range(c // 3 * 3, c // 3 * 3 + 3)]
        return s - set(rows) - set(cols) - set(boxes)

    def dfs(mtx: [[int]]):
        for i in range(9):
            idx = mtx[i].index(0) if 0 in mtx[i] else 0
            for j in range(idx, 9):
                if mtx[i][j] == 0:
                    for x in contains(mtx, i, j):  # range(1, 10):
                        mtx[i][j] = x
                        if check(mtx, i, j) and dfs(mtx):
                            return True
                        mtx[i][j] = 0
                    return False
        return True

    dfs(mtx)
    for e in mtx: print(*e)


def main():
    matrix = [list(map(int, input().split())) for _ in range(9)]
    my(matrix)


if __name__ == '__main__':
    main()
