"""
统计每个月兔子的总数_牛客题霸_牛客网
https://www.nowcoder.com/practice/1221ec77125d4370833fd3ad5ba72395

斐波那契数列
"""

from __init__ import redirect_input

redirect_input(__file__)


def main():
    n = int(input())
    if n <= 2:
        print(1)

    f1, f2 = 1, 1

    while n >= 3:
        n -= 1
        f2, f1 = f1 + f2, f2
    print(f2)


if __name__ == '__main__':
    main()
