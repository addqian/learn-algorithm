"""
公共子串计算_牛客题霸_牛客网
https://www.nowcoder.com/practice/98dc82c094e043ccb7e0570e5342dd1b

[hj65]
"""


def main():
    a = input()
    b = input()

    if len(a) > len(b): a, b = b, a

    common_len = 0

    for i in range(len(a) + 1):
        if a[i - common_len:i] in b:
            common_len += 1

    print(common_len - 1)


if __name__ == '__main__':
    main()
