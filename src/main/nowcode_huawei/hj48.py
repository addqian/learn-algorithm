"""
"""


def main():
    raw_list = list(map(int, input().split()))
    link_list = [raw_list[1]]

    node_list = raw_list[2:-1]
    i = 0
    while i < raw_list[0] - 1:
        idx = link_list.index(node_list[(i << 1) + 1]) + 1
        ele = node_list[i << 1]
        link_list.insert(idx, ele)
        i += 1
    link_list.remove(raw_list[-1])
    print(*link_list)


if __name__ == "__main__":
    main()
