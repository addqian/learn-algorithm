"""
表达式求值_牛客题霸_牛客网
https://www.nowcoder.com/practice/9566499a2e1546c0a257e885dfdbf30d



{I}
400+5
{O}
405

-1*(-1-1)
---
2

(7+4*3+10*(3-3+4-1-8-10))
---
-131

(7+4*3+10*(3-3+4-1-8-10))
2+(4*5+1)*8+7*4+10-9+3+1*9-(6-0*4*4)
---
205

5-3+9*6*(6-10-2)
---
-322

(1+20)*3
---
9
"""


def deal(exp):
    exp_list = list()

    temp = ''
    quote_left_depth = 0  # 括号深度
    quote_left_pos = 0  # 括号位置

    for i in range(len(exp)):
        if exp[i] == '(':
            quote_left_depth += 1
            if quote_left_depth == 1:
                quote_left_pos = i + 1
        elif exp[i] == ')':
            quote_left_depth -= 1
            if quote_left_depth == 0:
                exp_list.append(deal(exp[quote_left_pos:i]))
        elif quote_left_depth == 0:  # 没有括号的情况
            if exp[i].isdigit():
                temp += exp[i]
                if i == len(exp) - 1:
                    exp_list.append(int(temp))
                    temp = ''
                elif not exp[i + 1].isdigit():
                    exp_list.append(int(temp))
                    temp = ''
            else:
                exp_list.append(exp[i])

    operand_list = list()
    operator = ''
    for i in range(len(exp_list)):
        e = exp_list[i]
        if isinstance(e, int):
            if operator == '':
                operand_list.append(e)
            elif operator == '*':
                operand_list[-1] = operand_list[-1] * e
                operator = ''
            elif operator == '/':
                operand_list[-1] = operand_list[-1] / e
                operator = ''

        elif e == '*':
            operator = e
        elif e == '/':
            operator = e
        elif e == '+':
            operand_list.append(e)
        elif e == '-':
            operand_list.append(e)

    answer = 0
    operator = ''
    for i in range(len(operand_list)):
        e = operand_list[i]
        if isinstance(e, int):
            if operator == '':
                answer = e
            elif operator == '+':
                answer += e
            elif operator == '-':
                answer -= e
        elif e == '+':
            operator = e
        elif e == '-':
            operator = e

    return answer


def handle(exp: str) -> int:
    exp_list = list()

    idx = 0
    t = None
    while idx < len(exp):
        e = exp[idx]

        if e.isdigit():
            t = (t if t else 0) * 10 + int(e)
        else:
            if t is not None:
                exp_list.append(t)
                t = None
            exp_list.append(e)
        idx += 1

    if t is not None:
        exp_list.append(t)

    # print(exp_list)

    def calc_exp_list(exp_list: list) -> int:
        idx = 0
        q_l_depth = 0
        q_l_idx = 0
        while idx < len(exp_list):
            e = exp_list[idx]
            if e == '(':
                if q_l_depth == 0:
                    q_l_idx = idx
                q_l_depth += 1

            elif e == ')':
                q_l_depth -= 1
                if q_l_depth == 0:
                    exp_sub = calc_exp_list(exp_list[q_l_idx + 1: idx])
                    exp_list = exp_list[:q_l_idx] + exp_sub + exp_list[idx + 1:]
                    idx = q_l_idx

            idx += 1

        if exp_list[0] == '-':
            exp_list[1] = -exp_list[1]
            exp_list.pop(0)
        for op in ['*', '/', '-', '+']:
            while op in exp_list:
                idx = exp_list.index(op)
                a = exp_list[idx - 1]
                b = exp_list[idx + 1]
                if op == '*':
                    exp_list[idx - 1] = int(a) * int(b)
                elif op == '/':
                    exp_list[idx - 1] = int(a) / int(b)
                elif op == '+':
                    exp_list[idx - 1] = int(a) + int(b)
                elif op == '-':
                    exp_list[idx - 1] = int(a) - int(b)

                exp_list.pop(idx)
                exp_list.pop(idx)

        return exp_list

    res = calc_exp_list(exp_list)
    return res[0]


def main():
    expression = input()
    res = handle(expression)
    # res = deal(expression)
    print(eval(expression))
    print(res)


if __name__ == '__main__':
    main()
