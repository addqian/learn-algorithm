"""
MP3光标位置_牛客题霸_牛客网
https://www.nowcoder.com/practice/eaf5b886bd6645dd9cfb5406f3753e15
"""


def main():
    def up_down(cmds: str, end: int, start: int = 1, head: int = 1, tail: int = 4, idx: int = 1):
        if not cmds:
            return head, tail, idx

        if cmds[0] == 'D':
            if idx < tail:
                idx += 1
            elif idx < end:
                head += 1
                idx += 1
                tail += 1
            elif idx == end:
                head = 1
                tail = min(end, 4)
                idx = 1
            return up_down(cmds=cmds[1:], end=end, start=start, head=head, tail=tail, idx=idx)

        elif cmds[0] == 'U':
            if idx > head:
                idx -= 1
            elif idx > 1:
                head -= 1
                idx -= 1
                tail -= 1
            elif idx == 1:
                head = max(end - 4 + 1, 1)
                tail = end
                idx = end
            return up_down(cmds=cmds[1:], end=end, start=start, head=head, tail=tail, idx=idx)

    n = int(input())
    cmd_str = input()
    head, tail, idx = up_down(cmd_str, end=n)
    print(f'{" ".join([str(e) for e in range(head, tail + 1)])}\n{idx}')


if __name__ == '__main__':
    main()
