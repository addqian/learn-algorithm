def connect_input_filepath(filename):
    filename = filename.split('\\')[-1].split('.')[0]
    return f'./io/{filename}.input.txt'


def redirect_input(filename=__file__):
    from src.main.common_handle.python.io_handle import SysIORedirect
    SysIORedirect.i(connect_input_filepath(filename))
