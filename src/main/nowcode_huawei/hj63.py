"""
DNA序列_牛客题霸_牛客网
https://www.nowcoder.com/practice/e8480ed7501640709354db1cc4ffd42a
"""
from datetime import datetime
from time import time


def main():
    s = input()
    n = int(input())

    dp = [0 for _ in range(len(s))]
    dp[n - 1] = s[:n].count('C') + s[:n].count('G')
    idx = n - 1
    for i in range(n, len(s)):
        dp[i] = dp[i - 1]

        if s[i - n] in 'CG':
            dp[i] -= 1
        if s[i] in 'CG':
            dp[i] += 1

        if dp[i] > dp[idx]: idx = i

    print(s[idx - n + 1: idx + 1])


if __name__ == '__main__':
    main()
