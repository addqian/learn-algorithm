"""
The Longest Palindromic Substring
最长回文子串_牛客题霸_牛客网
https://www.nowcoder.com/practice/12e081cd10ee4794a2bd70c7d68f5507

⭕处理问题时, 考虑所有情况, 并测试

>>>[I][O]
abcbaaa
5

abba
4

ysuxvcjbpjxdcpkrfpolmeypyttqwvilvknedjwrznxvgrwtxdjzmxjcocclchsgkvvimrjdgsxjwhfmcbiqtjosfsqenetthaoypcnpwqaqgamsojtiwysiphqvagskxxdjdrmdrzuepageyplusyjiuhuiyuoqqhkkhqqouyiuhuijysulpyegapeuzrdmrdjdxxksgavqhpisywitjosmagqaqwpncpyoahtteneqsfsojtqibcmfhwjxsgdjrmivvkgshclccocjxmzjdxtwrgvxnzrwjdenkvlivwqttypyoblylsccuxmlfkvcattmtx
282
"""


def longest_palindrome1(s: str):
    """
    暴力法
    """
    l = [1]

    for i in range(1, len(s) + 1):
        for j in range(0, i):
            t = s[j : i + 1]
            if t == t[::-1]:
                l.append(len(t))

    return max(l)


def longest_palindrome2(s: str):
    """
    暴力法
    """
    l = 1

    for i in range(1, len(s) + 1):
        for j in range(l, i + 1):
            t = s[i - j : i + 1]
            if t == t[::-1]:
                l = j + 1

    return l


def longest_palindrome3(s: str):
    """
    Manacher 算法
    O(n)
    """
    # 预处理字符串
    s = "#" + "#".join(s) + "#"
    n = len(s)

    p = [0] * n         # p[i] 表示以 s[i] 为中心的最长回文子串的半径(包含 s[i] 本身)
    center = right = 0  # 分别表示当前已知的回文子串的中心和右边界
    max_length = 0      # 当前最长回文子串的长度
    max_center = 0      # 最长回文子串的中心位置

    for i in range(n):
        if i < right:
            mirror = 2 * center - i  # 计算 i 关于当前中心 center 的对称位置 mirror
            p[i] = min(right - i, p[mirror])

        # 尝试扩展
        l, r = i - (1 + p[i]), i + (1 + p[i])
        while l >= 0 and r < n and s[l] == s[r]:
            p[i] += 1
            l -= 1
            r += 1

        # 更新最大回文串的中心和右边界
        if i + p[i] > right:
            center, right = i, i + p[i]

        # 更新最长回文串的长度和中心
        if p[i] > max_length:
            max_length, max_center = p[i], i

    # 去除添加的特殊字符，得到原始最长回文串
    start = max_center - max_length >> 1
    # print(f'start: {start}')
    # print(f'max_length: {max_length}')
    # print(f'max_center: {max_center}')
    # print(f's: {s}')

    res= s.replace("#", "")[start : start + max_length]
    return len(res)
    # return max_length


def longest_palindrome4(s: str):
    """
    分类讨论
    1. even
    2. odd
    """
    s_len = len(s)
    sub_len_max = 0
    for i in range(1, s_len - 1):
        l_o = 0
        for j in range(0, min(i+1, s_len - i - 1)):
            if s[i - j] == s[i + j + 1]:
                l_o += 2
            else:
                break
        l_e = 1
        for j in range(1, min(i+1, s_len - i)):
            if s[i - j] == s[i + j]:
                l_e += 2
            else:
                break

        sub_len_max = max(sub_len_max, l_o, l_e)

    return sub_len_max


def main():
    s = input()
    res=longest_palindrome3(s)
    # res = t(s)
    print(res)


if __name__ == "__main__":
    main()
