"""
矩阵乘法计算量估算_牛客题霸_牛客网
https://www.nowcoder.com/practice/15e41630514445719a942e004edc0a5b
"""


def main2():
    n = int(input())

    matrix = []
    for _ in range(n): matrix.append(list(map(int, input().split())))

    expression = input()

    stack = []
    count = 0
    for _ in expression:
        if _ == ")":
            b, a = stack.pop(), stack.pop()
            x, y, z = matrix[a - 65][0], matrix[b - 65][1], matrix[a - 65][1]
            stack.pop()  # '('

            stack.append(b)  # 重新入栈
            matrix[b - 65] = [x, y]  # 覆写行列值
            count += x * y * z
        else:
            stack.append(ord(_))

    print(count)


def main():
    def expression_calc(expressions):
        calc_count = 0

        idx = 0
        while idx < len(expressions):
            if expressions[idx] != ')':
                idx += 1
                continue
            else:
                expressions.pop(idx)
                idx -= 1
                stack = []

                while idx >= 0 and expressions[idx] != '(':
                    stack.insert(0, expressions.pop(idx))
                    idx -= 1
                if expressions[idx] == '(':
                    m = []
                    while stack:
                        a = stack.pop(0)
                        b = stack.pop(0)
                        m = [a[0], b[1]]
                        calc_count += a[0] * a[1] * b[1]
                    expressions[idx] = m
        print(calc_count)

    n = int(input())

    matrix_list = []
    for _ in range(n): matrix_list.append(list(map(int, input().split())))

    expression_raw = list(input())

    expression_matrix = [
        matrix_list[ord(e) - ord('A')] if e.isupper() else e for e in expression_raw
    ]
    expression_calc(expression_matrix)


if __name__ == '__main__':
    main()
