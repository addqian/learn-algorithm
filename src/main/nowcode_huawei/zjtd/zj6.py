def func(n):
    retain = 1024 - n
    coin = [64, 16, 4, 1]
    cnt = 0
    for i in coin:
        cnt += retain // i
        retain = retain % i
    print(cnt)


def main():
    n = int(input())
    func(n)


if __name__ == "__main__":
    main()
