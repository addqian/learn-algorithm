"""
牛客网 - 找工作神器|笔试题库|面试经验|实习招聘内推，求职就业一站解决_牛客网
https://www.nowcoder.com/exam/test/85557555/detail?pid=16516564
输入例子：
2
helloo
wooooooow
输出例子：
hello
woow

[I]
15
yyybeettxjjjpppddsrxxxkkkyyyooowwwwwkyyxxppplllwwwiivvssnrvvvccclyydddhaaayiic
mmccchwwkkkccccooojxbzooeeyyyggxggswwwyyynnuuffsdsmmnuunmmuuvvzzyycctttijjjrrmtttvvckkkllquuyyykkkvvgggllhtw
dllloeeedddwwvvzzajkkkffffzzzrrrmvvvzzzgggl
ebbbbbiweeeujnnnrkkkeelllyppprrcwwlwweefcqqmpjjllwwgggxxxpwwwirdddootbbbjwnhhhcgggnnddddryyyjcosssbbbpppddxttggkkffttmmmccvvttppiiuueeggxxxzzroouekkhpppeeehhcummrrrgtqqeeogsppwwwuuucccnggg
uhhccceeegggghhha
mksluvvvjjjuuueyyjjrrtrrrrrrnnnooyiiiannndddwjjjtppucccmmmvyyziiiniixtttmmm
ssgggggllnnngggiiiaaakkhhhlssobkkiuuuyyyyyyyiiisfffwwtttjjjabzzxiimoooooobbttt
cccgqqqcccxxxgggqqqwwwbllcccxbhhhppguuueqttvvvlttjjzdfjjjoolbbaatttbbopppffxxxdddm
bbbkkkkkfzznnnvvvwwjjssccaaawbbbdauqqiiaaappggzjjjlllhcctuuulyyppprrjqttoovcnnpppeeecjllaaaffnnffaatttpppqsssiii
qaaxxjjjttasdduuxxpccbbbeeeijjmyyrrrllxxkkknnezzzqqsqqqxxayyeenvveejjbbcccssyzzznnnsspyyyllllllqqqoouoonuuuunnnmsssvvksccmmmtttppjjvwnssnntttcctmmyyirazibbiigggkkhrrhvvtttcceeuuccktttfffctt
rrrmmmaiinnvvvppbkwwggqpppcccbboovvkkkyyyrr
vvdeexxxbbbyyynlcccn
gggvfyyyaatzzzuuunnnnnaiipchhhrriiirrfffoohhhvvyy
hhnqvwppdddusssibbblasuupgguuunnnzziiizzzlllwwqqqhsnniinnoooehhhdddkkkmmmzhhqqqwfffddr
vvummmxxxppaaarrrzzeennmmqqqljeecccrpqqquuummjjjqqquuuyyyvviiicccbbimuucqqqnnnrrfffddhslllsssiiiwwwnnlllrrrzzbblwaxxvvvjjjbbbggeemsfglcxxnnnddcccuuuzzlllzzmxxxxxxkkkxxxfffddejjjuuqqxxooniiyyyzz
[O]
yybeetxjjpddsrxxkyyowwkyyxpplwwivvsnrvvclyydhaayiic
mmchwwkccojxbzooeyygxggswwynnuffsdsmmnuunmmuvvzyycttijjrmttvckklquuykkvgglhtw
dlloeedwwvzzajkkfzzrmvvzggl
ebbiweeujnnrkkellypprcwwlwwefcqqmpjjlwwgxxpwwirddotbbjwnhhcggnddryyjcossbppdxttgkkfttmccvttpiiueegxxzroouekkhppehhcummrgtqqeogsppwuucngg
uhhceeghha
mksluvvjuueyyjrrtrrnooyiianndwjjtppuccmvyyziiniixttm
ssgllnggiaakhhlssobkkiuuyiisffwttjabzzxiimoobtt
ccgqqcxxgqqwbllcxbhhpguueqttvlttjzdfjjolbbattboppfxxdm
bbkfzznvvwjjsccawbbdauqqiaapggzjjlhcctuulyyprrjqttovcnnpeecjllaffnffattpqssi
qaaxjjtasdduxxpccbeeijjmyyrllxkknezzqsqqxayyenvvejjbccsyzznsspyylqqouoonuunmssvksccmttpjjvwnssnttctmmyirazibbiggkhrrhvvtcceuuckttfctt
rrmaiinvvpbkwwgqppcbbovvkyyr
vvdeexbbynlccn
ggvfyyatzzunnaiipchhriirffohhvyy
hhnqvwppdussibblasuupggunnziizllwqqhsnninnoehhdkkmzhhqwffdr
vvummxpparrzeenmmqljeecrpqqummjqquyyviicbbimuucqqnrrfddhsllsiiwnnlrrzbblwaxxvjjbggemsfglcxxnddcuuzllzmxxkxxfddejjuqqxooniiyzz
"""


def func(s: str):
    s = list(s)

    lng = len(s)
    depth = [1] * lng
    for i in range(lng - 2, -1, -1):
        if s[i] == s[i + 1]:
            depth[i] = depth[i + 1] + 1

    i = 0
    while i < len(s):
        if depth[i] >= 3:
            for j in range(depth[i] - 3 + 1):
                s.pop(i)
                depth.pop(i)
        if depth[i] == 2:  # 从后往前
            if i >= 2 and depth[i - 2] == 2:
                s.pop(i)
                depth.pop(i)
            else: i += 1
        else:
            i += 1
    return ''.join(s)


def main():
    n = int(input())
    for i in range(n):
        s = input()
        print(func(s))


if __name__ == '__main__':
    main()
