"""
字符串字符匹配_牛客题霸_牛客网
https://www.nowcoder.com/practice/22fdeb9610ef426f9505e3ab60164c93
"""


def main():
    short = input()
    long = input()

    print('false' if any(c not in long for c in short) else 'true')


if __name__ == '__main__':
    main()
