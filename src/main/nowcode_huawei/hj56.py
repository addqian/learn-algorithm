"""
HJ56 完全数计算
https://www.nowcoder.com/practice/7299c12e6abb437c87ad3e712383ff84
"""
from __init__ import redirect_input

redirect_input(__file__)


def is_prefect(num):
    d = [1]

    h = int(num ** 0.5)
    if h * h == num:
        d.append(h)
    else:
        h += 1

    for i in range(2, h):
        if num % i == 0:
            d.extend([i, num // i])

    if sum(d) == num: return 1
    return 0


def main():
    num = int(input())
    print(sum([is_prefect(i) for i in range(2, num + 1)]))


def main1():
    num = int(input())
    print(len(list(filter(lambda x: x < num, [6, 28, 496, 8128]))))


if __name__ == '__main__':
    # 3
    main()
