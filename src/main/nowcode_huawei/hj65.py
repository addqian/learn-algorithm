"""
HJ65 查找两个字符串a,b中的最长公共子串
https://www.nowcoder.com/practice/181a1a71c7574266ad07f9739f791506

[I]
awaabb
aawbb
[O]
aa
"""


def handle3(a: str, b: str):
    """
    滑动窗口
    """
    n = 0
    s = ''
    if len(a) > len(b): a, b = b, a

    for i in range(len(a)):
        if a[i - n:i] in b:
            s = a[i - n:i]
            n += 1

    print(s)


def handle2(a: str, b: str):
    """
    滑动窗口
    """
    if len(a) > len(b): a, b = b, a
    i, idx, lng = 0, 0, 1
    while i < len(a) - lng:
        while a[i:i + lng] in b:
            idx = i
            lng += 1
        i += 1
    print(a[idx:idx + lng - 1])


def handle4(a: str, b: str):
    """
    动态规划, 该算法的时间复杂度为 O(n*m)
    """
    if len(a) > len(b): a, b = b, a

    # row, col = a, b
    dp = [[0] * (len(b) + 1) for _ in range(len(a) + 1)]

    idx = 0
    lng = 0
    for i in range(1, len(a) + 1):
        for j in range(1, len(b) + 1):
            if a[i - 1] == b[j - 1]:
                dp[i][j] = dp[i - 1][j - 1] + 1
                if dp[i][j] > lng:
                    lng = dp[i][j]
                    idx = i
            else:
                dp[i][j] = 0

    print(a[idx - lng:idx])


def main():
    a = input()
    b = input()

    handle2(a, b)


if __name__ == '__main__':
    main()
