"""
放苹果_牛客题霸_牛客网
https://www.nowcoder.com/practice/bfd8234bb5e84be0b493656e390bdebf

<<<[NOTE] 20240603204839
f(m,n)=f(m,n−1)+f(m−n,n)
1. 有一个空盘子
2. 所有盘子都不空, 即每个盘子至少有一个苹果


初始状态转移矩阵
    dp[n][m]
    dp[i][j] 表示 i 个盘子放 j 个苹果的放法
状态转移方程为
    dp[i][j]=dp[i−1][j]+(j−i<0?0:dp[i][j−i])

[I]
7 3
[O]
8
"""


def f(m, n):
    if m < 0:
        return 0
    elif m == 0:
        return 1
    elif n == 0:
        return 0

    return f(m, n - 1) + f(m - n, n)


def f2(m, n):
    if m == 0 or n == 1:
        return 1
    if m < n:
        return f2(m, m)

    return f2(m, n - 1) + f2(m - n, n)


def main():
    while True:
        try:
            m, n = map(int, input().split())
            print(f2(m, n))
        except:
            break


if __name__ == '__main__':
    main()
