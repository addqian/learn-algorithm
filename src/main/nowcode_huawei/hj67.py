"""
24点游戏算法_牛客题霸_牛客网
https://www.nowcoder.com/practice/fbc417f314f745b1978fc751a54ac8cb

3 9 2 8
---
true

1 3 1 5
---
true

2 9 9 5
---
false

1 5 5 5
---
true


7 2 1 10
---
true

"""


def handle(array: list):
    """
    ❌部分用例通不过
    """

    def dfs(ar: list, tar) -> bool:
        if len(ar) == 1: return abs(abs(ar[0]) - tar) <= 0.0001

        for i in range(len(ar)):
            ar = ar[1:] + [ar[0]]
            if (
                    dfs(ar[1:], tar + ar[0]) or
                    dfs(ar[1:], tar - ar[0]) or
                    dfs(ar[1:], tar * ar[0]) or
                    dfs(ar[1:], tar / ar[0])
            ):
                return True
        return False

    result_bool = dfs(array, 24)
    print(str(result_bool).lower())


def handle0(arr: list):
    def calc(ar):
        if len(ar) == 1: return abs(abs(ar[0]) - 24) < 0.0001

        for i in range(len(ar)):
            for j in range(i + 1, len(ar)):
                a, b = ar[i], ar[j]
                ar_tmp = ar[:i] + ar[i + 1:j] + ar[j + 1:]
                if calc([a + b] + ar_tmp): return True
                if calc([a - b] + ar_tmp): return True
                if calc([a * b] + ar_tmp): return True
                if b != 0:
                    if calc([a / b] + ar_tmp): return True
                if a != 0:
                    if calc([b / a] + ar_tmp): return True
        return False

    print(str(calc(arr)).lower())


def main():
    operands = list(map(int, input().split()))
    handle0(operands)


if __name__ == '__main__':
    main()
