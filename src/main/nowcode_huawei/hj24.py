"""
合唱队_牛客题霸_牛客网
https://www.nowcoder.com/practice/6d9d69e3898f45169a441632b325c7b4

# 知识点 动态规划 队列

[I]
8
186 186 150 200 160 130 197 200
[O]
4

[I]
124
16 103 132 23 211 75 155 82 32 48 79 183 13 91 51 172 109 102 189 121 12 120 116 133 79 120 116 208 47 110 65 187 69 143 140 173 203 35 184 49 245 50 179 63 204 34 218 11 205 100 90 19 145 203 203 215 72 108 58 198 95 116 125 235 156 133 220 236 125 29 235 170 130 165 155 54 127 128 204 62 59 226 233 245 46 3 14 108 37 94 52 97 159 190 143 67 24 204 39 222 245 233 11 80 166 39 224 12 38 13 85 21 47 25 180 219 140 201 11 42 110 209 77 136
[O]
95
"""


def handle_list(n: int, heights: list):
    def handle(ar: list):
        n = len(ar)
        counts = [0 for _ in range(n)]

        for i in range(1, n - 1):
            d = [counts[j] for j in range(i) if ar[j] < ar[i]]
            if len(d) == 0: continue
            counts[i] = max(d) + 1
        return counts

    ar_l = handle(heights)
    ar_r = handle(heights[::-1])[::-1]

    # print(dp_left)
    # print(dp_right)
    print(n - max([a + b for a, b in zip(ar_l, ar_r)]) - 1)


def handle_dp(n: int, heights: list):
    """
    DP 的顺序查找, 时间复杂度 O(N^2)
    """
    def idx_ar(ar: list, ele: int):
        for i, e in enumerate(ar):
            if e >= ele:
                ar[i] = ele
                return i

    def count_ar(ar: list):
        counts = [0 for _ in range(len(ar))]
        stacks = [ar[0]]

        for i in range(1, n):
            if ar[i] > stacks[-1]:
                counts[i] = len(stacks)
                stacks.append(ar[i])
            elif ar[i] == stacks[-1]:
                counts[i] = len(stacks[:-1])
            else:
                counts[i] = idx_ar(stacks, ar[i])

        return counts

    counts = count_ar(heights)

    heights.reverse()  # [::-1]
    counts_reverse = count_ar(heights)
    counts_reverse.reverse()

    # print(counts)
    # print(counts_reverse)

    print(n - max([counts[i] + counts_reverse[i] for i in range(n)])-1)


def main():
    """
    """
    n = int(input())
    heights = list(map(int, input().split()))
    handle_dp(n, heights)


if __name__ == '__main__':
    main()
