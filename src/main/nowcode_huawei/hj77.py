"""
火车进站_牛客题霸_牛客网
https://www.nowcoder.com/practice/97ba57c35e9f4749826dc3befaeae109

[I]
3
1 2 3

[O]
1 2 3
1 3 2
2 1 3
2 3 1
3 2 1
"""


def handle(trains: list) -> list[str]:
    def dfs(station: list, in_station: list[int], out_station: list):
        if not station and not in_station:
            return res.append(' '.join(out_station))

        if station:  # IN STATION
            dfs(station[1:], in_station + [station[0]], out_station)
        if in_station:  # OUT STATION
            dfs(station, in_station[:-1], out_station + [in_station[-1]])

    res = []  # GLOBAL VAR
    dfs(trains, [], [])
    res = sorted(res)

    return res


def main():
    n = int(input())
    trains = input().split(' ')

    res = handle(trains)
    print(*res, sep='\n')


if __name__ == '__main__':
    main()
