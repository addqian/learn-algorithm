"""
"""


def main():
    s = input()

    score = 0
    l = len(s)

    if l <= 4:
        score = 5
    elif 5 <= l <= 7:
        score = 10
    elif l >= 8:
        score = 25

    has_lower = any(c.islower() for c in s)
    has_upper = any(c.isupper() for c in s)
    has_alpha = any(c.isalpha() for c in s)
    count_digit = [c.isdigit() for c in s].count(True)
    count_symbol = [c in '!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~' for c in s].count(True)

    if has_lower and has_upper:
        score += 20
    elif has_alpha:
        score += 10

    if count_digit > 1:
        score += 20
    elif count_digit == 1:
        score += 10

    if count_symbol > 1:
        score += 25
    elif count_symbol == 1:
        score += 10

    if has_upper and has_lower and count_digit > 0 and count_symbol > 0:
        score += 5
    elif has_alpha and count_digit > 0 and count_symbol > 0:
        score += 3
    elif has_alpha and count_digit > 0:
        score += 2

    if score >= 90:
        print('VERY_SECURE')
    elif score >= 80:
        print('SECURE')
    elif score >= 70:
        print('VERY_STRONG')
    elif score >= 60:
        print('STRONG')
    elif score >= 50:
        print('AVERAGE')
    elif score >= 25:
        print('WEAK')
    else:
        print('VERY_WEAK')


if __name__ == '__main__':
    main()
