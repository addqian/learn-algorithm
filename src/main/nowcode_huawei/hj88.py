"""
扑克牌大小_牛客题霸_牛客网
https://www.nowcoder.com/practice/d290db02bacc4c40965ac31d16b1c3eb
"""


def main():
    puke_dict = {'3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9, '10': 10, 'J': 11, 'Q': 12,
                 'K': 13, 'A': 14, '2': 15, 'joker': 16, 'JOKER': 17}

    def cmp(a, b): return a if puke_dict[a[0]] > puke_dict[b[0]] else b

    def concat(a): return ' '.join(a)

    def puke(s):
        if 'joker JOKER' in s:
            return 'joker JOKER'

        a, b = [e.split() for e in s.split('-')]
        if len(a) == 4 or len(b) == 4:
            if not (len(a) == 4 and len(b) == 4):
                if len(a) != 4: a = b
                return concat(a)

            return concat(cmp(a, b))

        elif len(a) != len(b):
            return 'ERROR'

        return concat(cmp(a, b))

    _s = input()
    print(puke(_s))


if __name__ == '__main__':
    main()
