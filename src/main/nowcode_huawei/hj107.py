"""
HJ107 求解立方根
求解立方根_牛客题霸_牛客网
https://www.nowcoder.com/practice/caf35ae421194a1090c22fe223357dca?tpId=37

说明:
    保留两位小数, 即精度 0.01
输入：
19.9
输出：
2.7
输入：
2.7
输出：
1.4
"""


def cube_root(num: float):
    cr = num * (0.5 if abs(num) > 1 else 2)

    while True:
        r3 = cr * cr * cr
        diff = num - r3
        diff_abs = abs(diff)

        if diff_abs < 0.01: break
        elif diff_abs > 1: b = 0.1
        elif diff_abs > 0.1: b = 0.01
        else: b = 0.001

        cr += b * (1 if diff > 0 else -1)

    return f'{cr:.2f}'


def main():
    num = float(input())
    res = cube_root(num)
    print(res)


if __name__ == '__main__':
    main()
