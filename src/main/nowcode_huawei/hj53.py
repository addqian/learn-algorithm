"""
杨辉三角的变形_牛客题霸_牛客网
https://www.nowcoder.com/practice/8ef655edf42d4e08b44be4d777edbf43
"""

from __init__ import redirect_input

redirect_input(__file__)


def main():
    """
    Run timeout When Figure Out The Yang Hui Triangle
    Returns:
    """

    def get(ar: [], idx: int):
        if idx < 0 or idx >= len(ar):
            return 0
        return ar[idx]

    n = int(input())
    if n <= 2:
        print(-1)
        return

    ar = [1, 1, 1]
    ar_next = [1]

    for i in range(3, n + 1):
        for j in range(1, (i - 1) << 1):
            ar_next.append(get(ar, j - 2) + get(ar, j - 1) + get(ar, j))
        ar_next.append(1)
        # print(" ".join(map(str, ar)))
        ar = ar_next
        ar_next = [1]

    for i in range(len(ar)):
        if ar[i] & 1 == 0:
            print(i + 1)
            return
    print(-1)


def main2():
    while True:
        try:
            n = int(input())
            if n <= 2:
                res = -1
            elif n & 1 == 1:
                res = 2
            elif n & 2 == 0:
                res = 3
            else:
                res = 4
            print(res)
        except:
            break


if __name__ == '__main__':
    main2()
