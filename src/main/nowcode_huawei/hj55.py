"""
挑7_牛客题霸_牛客网
https://www.nowcoder.com/practice/ba241b85371c409ea01ac0aa1a8d957b

输入：
20
输出：
3
说明：输入20，1到20之间有关的数字包括7,14,17共3个。

9652
---
4195

198
---
61

1084
---
400
"""


def handle2(n: int):
    MAX_NUM = 30000
    DIGIT = len(str(MAX_NUM)) - 2

    def get_count_of_seven(n):
        seven_list = []

        # 7 的倍数
        for i in range(7, n + 1, 7):
            seven_list.append(i)

        # 个位是 7
        for i in range(7, n + 1, 10):
            seven_list.append(i)

        # 十位是 7, 百位是 7, 千位是 7......
        for d in [10 ** i for i in range(DIGIT)]:
            for flag in [(80 + 100 * j) * d for j in range(MAX_NUM // (100 * d))]:
                tmp = n + 1 if n < flag else flag
                for i in range(flag - 10 * d, tmp):
                    seven_list.append(i)

        seven_list = list(set(seven_list))
        seven_list = sorted(seven_list)
        print(seven_list)
        return len(seven_list)

    count = get_count_of_seven(n)
    return count


def handle(n):
    count = 0
    for i in range(1, n + 1):
        if '7' in str(i) or i % 7 == 0:
            count += 1
    return count


def handle3(n):
    seven_list = set()

    for i in range(7, n + 1, 7):
        seven_list.add(i)

    digit = 1
    while n // digit > 0:
        digit *= 10

        for i in range(0, n + 1):
            t = i*digit+7*(digit//10)
            if t > n:
                break
            seven_list.add(t)
            for j in range(0, digit//10):
                if t + j < n + 1:
                    seven_list.add(t + j)
                else:
                    break

    seven_list = sorted(seven_list)
    print(seven_list)
    return len(seven_list)


def main():
    n = int(input())
    res = handle3(n)
    print(res)


if __name__ == '__main__':
    main()
