"""
名字的漂亮度_牛客题霸_牛客网
https://www.nowcoder.com/practice/02cb8d3597cf416d9f6ae1b9ddc4fde3"""


def main():
    def calc_perfect(s):
        d = {}
        for i in s:
            if i in d:
                d[i] += 1
            else:
                d[i] = 1
        d = sorted(d.values(), key=lambda x: x, reverse=True)
        print(sum([(26 - i) * d[i] for i in range(len(d))]))

    n = int(input())
    for i in range(n):
        calc_perfect(input())


if __name__ == '__main__':
    main()
