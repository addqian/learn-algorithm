"""
称砝码_牛客题霸_牛客网
https://www.nowcoder.com/practice/f9a4c19050fc477e9e27eb75f3bfd49c

1. 位移法   1<<1|1<<1
2. 动态规划
3. 迭代法


IO
2
1 2
2 1
---
5


10
2000 1999 1998 1997 1996 1995 1994 1993 1992 1991
10 10 10 10 10 10 10 10 10 10
---
16601
"""


def handle(n: int, weight: list[int], number: list[int]):
    # process 1994'ms, about 1.994's time consuming
    b = 1

    for i, e in enumerate(weight):
        for _ in range(number[i]):
            b |= b << e
            # print(bin(b).count('1'))

    return bin(b).count('1')


def handle1(n, weight, number):
    # process 663302'ms, about 663.302's time consuming
    all = []
    for i, e in enumerate(weight):
        all.extend([e] * number[i])

    res = [0]
    for i, e in enumerate(all):
        res_t = [e + r for r in res if e+r not in res]
        res.extend(res_t)
    return len(res)


def handle2(n, weight, number):
    # process 476046'ms, about 476.046's time consuming
    max_weight = sum(w*n for w, n in zip(weight, number))
    dp = [False]*(max_weight+1)
    dp[0] = True

    for i in range(n):

        for d in range(max_weight, -1, -1):
            if dp[d]:
                for j in range(1, number[i]+1):
                    if d+weight[i]*j <= max_weight:
                        dp[d+weight[i]*j] = True
    return sum(dp)


def main():
    # input
    n = int(input())
    weight = list(map(int, input().split()))
    number = list(map(int, input().split()))
    # handle
    res = handle(n, weight, number)
    # output
    print(res)


if __name__ == '__main__':
    main()
