'''
服务器, 接收文件
pip install flask
'''

from flask import Flask, request

app = Flask(__name__)


@app.route("/")
def index(): return "<h1>index</h1>"


@app.route("/u", methods=["POST"])
def receive_save_file():
    try:
        data = request.files
        file = data["file"]
        # print(type(data))
        # print(data)
        # print(file.filename)
        # print(type(request.headers))
        # print(request.headers)
        # print(request.headers.get("File-Name"))

        # 文件写入磁盘
        file.save(file.filename)

        return "upload success"
    except Exception as e:
        return "upload failed"


if __name__ == "__main__":
    app.run(host="0.0.0.0")

