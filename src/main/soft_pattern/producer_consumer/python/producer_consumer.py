# 实现生产者消费者模式

import multiprocessing as mp
import time


# 生产者, 生产产品
def producer(q, name, items):
    for item in items:
        q.put(item)
        print(f"{name}: 生产了 {item}")
        time.sleep(1)  # 休眠 1 秒


# 消费者, 消费产品
def consumer(q, name):
    while True:

        if not q.empty():  # 队列不为空时才消费
            item = q.get()
            print(f"{name}: 消费了 {item}")
            time.sleep(1)  # 休眠 1 秒

        else:
            print(f"{name}: 队列为空, 等待新数据...")
            time.sleep(3)  # 等待生产者生产产品


if __name__ == '__main__':
    # 创建一个共享队列
    queue = mp.Queue()

    # 创建生产者进程和消费者进程
    producer_process = mp.Process(target=producer, args=(queue, '生产者', ['商品 1', '商品 2', '商品 3']))
    consumer_process = mp.Process(target=consumer, args=(queue, '消费者'))

    # 启动进程
    producer_process.start()
    consumer_process.start()

    # 确保生产者先结束, 然后消费者可以安全退出
    producer_process.join()

    # 停止消费者, 队列已经空了(所有产品都已经消费完)
    # 如果想要消费者持续运行直到手动停止程序, 可以注释掉下面这行
    consumer_process.terminate()