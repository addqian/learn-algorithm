package soft_pattern.design_pattern.dp_2_structural;

/**
 * 装饰模式
 */

// 接口
interface Shape {
    void draw();
}

// 接口实现-圆
class CircleShape implements Shape {
    @Override
    public void draw() { System.out.println("CircleShape"); }
}

// 接口实现-三角形
class TriangleShape implements Shape {
    @Override
    public void draw() { System.out.println("TriangleShape"); }
}

// 装饰抽象类
abstract class DecoratorShape implements Shape {
    protected Shape shape;

    public DecoratorShape(Shape shape) { this.shape = shape; }

    @Override
    public void draw() { shape.draw(); }
}

// 装饰抽象类的实现-红色
class RedDecoratorShape extends DecoratorShape {
    public RedDecoratorShape(Shape shape) { super(shape); }

    @Override
    public void draw() {
        System.out.print("red-");
        this.shape.draw();
    }
}

// 装饰抽象类的实现-绿色
class GreenDecoratorShape extends DecoratorShape {
    public GreenDecoratorShape(Shape shape) { super(shape); }

    @Override
    public void draw() {
        System.out.print("red-");
        this.shape.draw();
    }
}


public class DecoratorPattern {
    public static void main(String[] args) {
        CircleShape circleShape = new CircleShape();
        TriangleShape triangleShape = new TriangleShape();

        new RedDecoratorShape(circleShape).draw();
        new GreenDecoratorShape(circleShape).draw();
        new RedDecoratorShape(triangleShape).draw();
        new GreenDecoratorShape(triangleShape).draw();
    }
}