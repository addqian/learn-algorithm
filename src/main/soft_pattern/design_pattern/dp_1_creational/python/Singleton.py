import threading

'''
单例模式: 双重校验
'''


class Singleton:
    _instance = None
    _lock = threading.Lock()

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            with cls._lock:
                if not cls._instance:
                    cls._instance = super().__new__(cls, *args, **kwargs)
        return cls._instance


if __name__ == '__main__':
    inst1 = Singleton()
    inst2 = Singleton()
    # True
    print(inst1 is inst2)
