package soft_pattern.design_pattern.dp_1_creational.java;


class IDGenEager {
    private static volatile IDGenEager instance = new IDGenEager();

    private IDGenEager() {}

    public static IDGenEager getInstance() {return instance;}
}

class IDGenLazy {
    private static volatile IDGenLazy instance;

    /**
     * make the constructor private so that this class cannot be instantiated
     */
    private IDGenLazy() {}

    /**
     * get unique instance in the process
     */
    public static IDGenLazy getInstance() {
        if (instance == null) {
            /** synchronized to prevent multi thread access to the instance */
            synchronized (IDGenLazy.class) {
                if (instance == null) {
                    instance = new IDGenLazy();
                }
            }
        }
        return instance;
    }
}


public class SingletonPattern {
    public static void main(String[] args) {
        IDGenLazy idGenerator1 = IDGenLazy.getInstance();
        IDGenLazy idGenerator2 = IDGenLazy.getInstance();
        System.out.println(idGenerator1 == idGenerator2);
    }
}