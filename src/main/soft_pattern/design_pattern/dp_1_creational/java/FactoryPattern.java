package soft_pattern.design_pattern.dp_1_creational.java;

/**
 * The purpose of the factory pattern is to hide the object creation process
 */
interface Product {
    void show();
}

class ProductImplA implements Product {
    public void show() {System.out.println(this.getClass().getName());}
}

class ProductImplB implements Product {
    public void show() {System.out.println(this.getClass().getName());}
}

class ProductFactory {
    public static Product createProduct(String type) {
        Product product = null;
        if ("A".equalsIgnoreCase(type)) {
            product = new ProductImplA();
        } else if ("B".equalsIgnoreCase(type)) {
            product = new ProductImplB();
        }
        return product;
    }
}

public class FactoryPattern {
    public static void main(String[] args) {
        Product productA = ProductFactory.createProduct("A");
        productA.show();

        Product productB = ProductFactory.createProduct("B");
        productB.show();
    }
}