package soft_pattern.design_pattern.dp_3_behavioral;

interface Strategy {
    void execute();
}

class StrategyImplA implements Strategy {
    @Override
    public void execute() {System.out.println("Strategy A");}
}

class StrategyImplB implements Strategy {
    @Override
    public void execute() {System.out.println("Strategy B");}
}

class Context {
    private Strategy strategy;

    public void setStrategy(Strategy strategy) {this.strategy = strategy;}

    public void executeStrategy() {strategy.execute();}
}


public class StrategyPattern {
    public static void main(String[] args) {
        Context context = new Context();

        context.setStrategy(new StrategyImplA()); // set the policy object
        context.executeStrategy(); // execute the method of the policy object

        context.setStrategy(new StrategyImplB());
        context.executeStrategy();
    }
}