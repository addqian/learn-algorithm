""""
面向对象开发
    1. 封装
    2. 继承
    3. 多态
"""


class Animal:  # 封装
    def __init__(self, name, category):
        self.name = name
        self.category = category

    def yell(self):  # 动物叫声
        pass


class Dog(Animal):  # 继承
    def __init__(self, name, age):
        super().__init__(name, "dog")
        self.age = age

    def yell(self):
        print("woof!")


class Cat(Animal):
    def __init__(self, name, age):
        super().__init__(name, "cat")
        self.age = age

    def yell(self):
        print("meow!")


class Pet():  # 多态
    def __init__(self, animal):
        self.animal = animal

    def yell(self):
        self.animal.yell()


if __name__ == '__main__':
    # 创建 Dog
    dog = Dog("spike", 3)

    # 创建 Cat
    cat = Cat("tom", 3)

    # 创建 Pet, 将 Dog 类的实例作为参数传递
    pet = Pet(dog)  # 多态
    pet.yell()
