import time


def timer(description=""):
    def decorator(func):
        def wrapper(*args, **kwargs):
            start_time = time.time()
            result = func(*args, **kwargs)
            end_time = time.time()
            print(f"{description}: {func.__name__}() 运行耗时 {end_time - start_time} 秒")
            return result

        return wrapper

    return decorator


# 使用装饰器
@timer(description="计时器")
def handle(n):
    time.sleep(n)


if __name__ == "__main__":
    handle(3)