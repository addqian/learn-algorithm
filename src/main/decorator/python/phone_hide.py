# 隐藏电话号码
def phone_hider(start=3, length=4):
    def decorator(func):
        def wrapper(*args, **kwargs):
            phone_num = args[0]
            phone_head = phone_num[:start]
            phone_tail = phone_num[start + length:]
            phone_waist = '*' * length
            phone_num = f'{phone_head}{phone_waist}{phone_tail}'

            args = (phone_num,)
            result = func(*args, **kwargs)
            return result

        return wrapper

    return decorator


@phone_hider(start=3)
def phone_hide(phone_num):
    return phone_num


if __name__ == "__main__":
    phone_num = '12345678901'
    phone_hidden = phone_hide(phone_num)
    print(phone_hidden)