# 数组分组_牛客题霸_牛客网
# https://www.nowcoder.com/practice/9af744a3517440508dbeb297020aca86


def array_group(ar, index, target):
    if index >= len(ar): return target == 0
    return array_group(ar, index + 1, target - ar[index]) or array_group(ar, index + 1, target)


def main():
    n = int(input())
    ar = list(map(int, input().split()))

    s = 0
    s5 = 0
    s3 = 0
    ar0 = []

    for e in ar:
        s += e
        if e % 5 == 0:
            s5 += e
        elif e % 3 == 0:
            # s3 += e
            pass
        else:
            ar0.append(e)

    if (s & 1) == 1:
        print('false')
    else:
        print('ture' if array_group(ar0, 0, (s >> 1) - s5) is True else 'false')


if __name__ == '__main__':
    import sys

    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        while True:
            try:
                main()
            except EOFError as e:
                break