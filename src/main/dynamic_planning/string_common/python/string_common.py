def main():
    a = input()
    b = input()

    if a == b:
        print(len(a))
        return

    if len(a) > len(b):
        a, b = b, a

    ar = [0] * len(a)

    for i in range(len(a)):
        for j in range(ar[i - 1] + 1):
            if a[i - j:i+1] in b:
                ar[i] = j + 1
            else:
                break

    print(max(ar))


if __name__ == '__main__':
    import sys

    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        while True:
            try:
                main()
            except EOFError as e:
                break