# 0-1's Knapsack Problem

import sys


def main():
    # N: 物品数量, V: 背包最大负重
    N, V = map(int, input().split())

    # 物品价值, 物品重量
    values, weights = [0 for _ in range(N)], [0 for _ in range(N)]
    for n in range(N): values[n], weights[n] = map(int, input().split())

    # 背包容量 1->V, 初始状态 values[0]|0
    dp = [values[0] if i + 1 >= weights[0] else 0 for i in range(V)]

    for n in range(N):
        for v in range(V - 1, weights[n] - 1, -1):
            dp[v] = max(dp[v], dp[v - weights[n]] + values[n])

    # 19
    print(dp[- 1])


if __name__ == '__main__':
    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        main()
