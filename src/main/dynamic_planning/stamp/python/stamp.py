import sys


def main():
    # M: 总值, N: 邮票数量
    M, N = int(input()), int(input())
    # stamps = [0 for _ in range(N)]
    stamps = list(map(int, input().split()))

    # stamp_iterate(M, N, stamps)
    stamp_dp(M, N, stamps)


def stamp_dp(M, N, stamps):
    dp = [N + 1 for _ in range(M + 1)]
    dp[0] = 0

    for i in range(N):
        for j in range(M, stamps[i] - 1, -1):
            dp[j] = min(dp[j], dp[j - stamps[i]] + 1)

    print(dp[-1] if dp[-1] != N + 1 else 0)


def stamp_iterate(M, N, stamps):
    global min
    min = N + 1
    compute(M, stamps, 0, N - 1, 0)
    print(0 if min > N else min)


def compute(M, stamps, sum_num, n, num):
    # m 是递归中几张邮票面值的和, n 表示 stamps[] 还未参与递归的最高位, sum 由 num 张邮票相加得到
    global min
    if sum_num == M:
        min = (num if num <= min else min)
        return
    if sum_num > M:
        return
    for i in range(n, -1, -1):
        compute(M, stamps, sum_num + stamps[i], i - 1, num + 1)


# Stamp Problem
if __name__ == '__main__':
    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        main()
