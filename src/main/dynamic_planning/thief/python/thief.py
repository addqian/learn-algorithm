# thief problem
import sys


def main():
    values = list(map(int, input().split()))
    dp = [0 for _ in range(len(values))]
    dp[0] = values[0]
    dp[1] = max(values[0], values[1])

    for i in range(2, len(values)): dp[i] = max(dp[i - 2] + values[i], dp[i - 1])

    print(dp[-1])


if __name__ == '__main__':
    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        while True:
            try:
                main()
            except EOFError:
                break
