# 自动售货系统_牛客题霸_牛客网
# https://www.nowcoder.com/practice/cd82dc8a4727404ca5d32fcb487c50bf


def main():
    goods = {
        "A1": [2, 0],
        "A2": [3, 0],
        "A3": [4, 0],
        "A4": [5, 0],
        "A5": [8, 0],
        "A6": [6, 0],
    }

    coins = {"1": 0, "2": 0, "5": 0, "10": 0}

    commands = input().split(";")
    c_init = [list(map(int, c.split("-"))) for c in commands[0][2:].split(" ")]
    goods["A1"][1] = c_init[0][0]
    goods["A2"][1] = c_init[0][1]
    goods["A3"][1] = c_init[0][2]
    goods["A4"][1] = c_init[0][3]
    goods["A5"][1] = c_init[0][4]
    goods["A6"][1] = c_init[0][5]

    coins["1"] = c_init[1][0]
    coins["2"] = c_init[1][1]
    coins["5"] = c_init[1][2]
    coins["10"] = c_init[1][3]

    print('S001:Initialization is successful')
    coins_keys = coins.keys()

    commands = commands[1:-1]

    balance = 0
    for c in commands:
        if c[0] == "p":  # 投币
            coin = c.split()[1]
            if coin not in coins_keys:
                print('E002:Denomination error')
                continue
            if coin not in ('1', '2') and (coins["1"] * 1 + coins["2"] * 2) < int(coin):
                print('E003:Change is not enough, pay fail')
                continue
            if sum([good[1] for good in goods.values()]) == 0:
                print('E005:All the goods sold out')
                continue

            balance += int(coin)
            coins[coin] += 1
            print(f'S002:Pay success,balance={balance}')

        elif c[0] == "b":  # 购买商品
            b = c.split()[1]
            if b not in goods.keys():
                print('E006:Goods does not exist')
            elif goods[b][1] == 0:
                print('E007:The goods sold out')
            elif balance < goods[b][0]:
                print('E008:Lack of balance')
            else:
                balance -= goods[b][0]
                print(f'S003:Buy success,balance={balance}')
        elif c[0] == "c":  # 退币
            if balance == 0:
                print('E009:Work failure')
            else:
                c10 = balance // 10
                balance -= c10 * 10
                coins["10"] -= c10
                c5 = balance // 5
                balance -= c5 * 5
                coins["5"] -= c5
                c2 = balance // 2
                balance -= c2 * 2
                coins["2"] -= c2
                c1 = balance // 1
                balance -= c1 * 1
                coins["1"] -= c1
                print(f'1 yuan coin number={c1}\n2 yuan coin number={c2}\n5 yuan coin number={c5}\n10 yuan coin number={c10}')
        elif c[0] == "q":  # 查询
            if c[1] != ' ':
                print('E010:Parameter error')
            elif c[2:] == '0':  # 商品
                print(f"A1 {goods['A1'][0]} {goods['A1'][1]}\nA2 {goods['A2'][0]} {goods['A2'][1]}\nA3 {goods['A3'][0]} {goods['A3'][1]}\nA4 {goods['A4'][0]} {goods['A4'][1]}\nA5 {goods['A5'][0]} {goods['A5'][1]}\nA6 {goods['A6'][0]} {goods['A6'][1]}")
            elif c[2:] == '1':  # 零钱
                print(f"1 yuan coin number={coins['1']}\n2 yuan coin number={coins['2']}\n5 yuan coin number={coins['5']}\n10 yuan coin number={coins['10']}")

            else:
                print('E010:Parameter error')


if __name__ == '__main__':
    import sys

    with open('../i', "r") as fr:
        sys.stdin = fr
        while True:
            try:
                main()
            except EOFError as e:
                break
