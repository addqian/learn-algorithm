# 扑克牌大小_牛客题霸_牛客网
# https://www.nowcoder.com/practice/d290db02bacc4c40965ac31d16b1c3eb

def main():
    s = input()
    a, b = [ar.split() for ar in s.split("-")]

    if 'joker JOKER' in s:
        print('joker JOKER')
    elif len(a) == 4:
        print(*a)
    elif len(b) == 4:
        print(*b)
    elif len(a) == len(b):
        dt = {
            '3': 0,
            '4': 1,
            '5': 2,
            '6': 3,
            '7': 4,
            '8': 5,
            '9': 6,
            '10': 7,
            'J': 8,
            'Q': 9,
            'K': 10,
            'A': 11,
            '2': 12,
            'joker': 13,
            'JOKER': 14
        }
        print(*(a if dt[a[0]] > dt[b[0]] else b))
    else:
        print('ERROR')


if __name__ == '__main__':
    import sys

    with open('../input.txt', "r") as fr:
        sys.stdin = fr
        while True:
            try:
                main()
            except EOFError:
                break
