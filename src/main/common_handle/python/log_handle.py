import logging.handlers

formatter = logging.Formatter(
    fmt='%(asctime)s %(levelname)s %(filename)s.%(funcName)s().%(lineno)s %(message)s'
    , datefmt='%Y-%m-%d %H:%M:%S'
)

logger = logging.getLogger('main')
logger.setLevel(logging.INFO)

# console log handler
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(formatter)

# add console handler
if console_handler not in logger.handlers:
    logger.addHandler(console_handler)


def add_file_handle(log_file='main.log', log_level=logging.INFO):
    file_handler = logging.FileHandler(filename=log_file)
    file_handler.setLevel(log_level)
    file_handler.setFormatter(formatter)

    # add file handler
    if file_handler not in logger.handlers:
        logger.addHandler(file_handler)


if __name__ == '__main__':
    logger.info('info log')
