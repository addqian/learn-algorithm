#!/usr/bin/env python3
"""
@Project    ：learn-algorithm 
@File       ：io_handle.py 
@Author     ：qqq
@Time       ：2024/4/14 12:17
@Annotation : “”
"""
import os
import sys

"""
输入输出重定向
"""


class SysIORedirect:
    @staticmethod
    def check_filepath(filepath):
        if not filepath:
            raise ValueError("filepath is empty")

        if not os.path.exists(filepath):
            with open(filepath, "w") as fw: fw.write("")
            raise ValueError(f"file {filepath} not exists but create now")

    @staticmethod
    def i(filepath):
        SysIORedirect.check_filepath(filepath)
        sys.stdin = open(filepath, 'r')

    @staticmethod
    def o(filepath):
        SysIORedirect.check_filepath(filepath)
        sys.stdout = open(filepath, 'w')
