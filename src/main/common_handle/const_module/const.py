class _const:
    class ConstError(TypeError): pass

    def __setattr__(self, key, value):
        # self.__dict__
        if key in self.__dict__:
            data = "Can't rebind const (%s)" % key
            raise self.ConstError(data)
        self.__dict__[key] = value


import sys

# 将当前模块(__name__)替换为一个 _const 类的实例, 因此不能在这个模块内重新赋值任何变量, 因为所有赋值操作都会被 __setattr__ 方法拦截并阻止
# Replace the __name__ of the current module with an instance of the _const class
# Therefore, it is not possible to re-assign any variables within this module, as all assignments will be intercepted and blocked by the __setattr__ method
sys.modules[__name__] = _const()
