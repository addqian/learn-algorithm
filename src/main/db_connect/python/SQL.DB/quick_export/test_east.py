from autil.db_util import DB

db_east = DB()
db_east = db_east.oracle_east_test_connect()

db_fht = DB()
db_fht = db_fht.fht_connect()

list_fht = ['IE4_006_BFMXB', 'IE4_006_TTBFB', 'IE4_006_FFMXB', 'IE4_005_BDYJXXB', 'IE4_005_GRBDB', 'IE4_008_LPBDMXB',
            'IE4_008_CXRXXB', 'IE4_005_BDXSRYGLB', 'IE4_005_KHHFB', 'IE4_005_GRXZB', 'IE4_005_BBXRB', 'IE4_005_TTXZB',
            'IE4_005_TTBDB']
list_east = ['BFMXB', 'TTBFB', 'FFMXB', 'BDYJXXB', 'GRBDB', 'LPBDMXB', 'CXRXXB', 'BDXSRYGLB', 'KHHFB', 'GRXZB', 'BBXRB',
             'TTXZB', 'TTBDB']

list_field = [
    'LSH 流水号, BXJGDM 保险机构代码, BXJGMC 保险机构名称, TTBDH 团体保单号, GRBDH 个人保单号, BDTGXZ 保单团个性质, GDBXXZHM 个单保险险种号码, CPBM 产品编码, SFHM 实付号码, FFLX 付费类型, FFFS 付费方式, SFJE 实付金额, DZRQ 到账日期, QRRQ 确认日期, YXMC 银行名称, YXZH 银行账号, YXZHMC 银行账户名称, ZJLX 证件类型, ZJHM 证件号码, LPPAH 理赔赔案号, CJRQ 采集日期'
    , ]

CJRQ = "20230930"

if __name__ == '__main__':
    for i in range(13):
        print(f'process {i}')

        FFRO = "FETCH FIRST 10 ROWS ONLY"
        FFRO = ""

        table_east = db_east.get(f'''''')
        table_fht = db_fht.get(f'''''')

        # 1. 条数
        print(f"\tlen diff: {len(table_east) - len(table_fht)}")

        table_east_lsh = [e[0] for e in table_east]
        table_fht_lsh = [e[0] for e in table_fht]

        # 2. lsh
        print(f"\tlsh diff: {list(set(table_east_lsh) - set(table_fht_lsh))}")

        # 3. 字符串
        # print(f'\tdetail: {[]}')
