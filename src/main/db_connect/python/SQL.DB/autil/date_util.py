import datetime


def get_last_month_end():
    today = datetime.date.today()
    last_day = datetime.date(today.year, today.month, 1) - datetime.timedelta(1)
    return last_day


def get_last_month_first_last_day():
    """
    获取上月第一天和最后一天并返回(元组)
    example:
        now date:2020-03-06
        return:(2020-02-01,2020-02-29)
    :return:
    """
    today = datetime.date.today()
    last_month_last_day = datetime.date(today.year, today.month, 1) - datetime.timedelta(1)
    last_month_first_day = datetime.date(last_month_last_day.year, last_month_last_day.month, 1)
    return last_month_first_day, last_month_last_day


def get_this_year_first_day():
    """
        return:2020-01-01
    :return:
    """
    today = datetime.date.today()
    this_year_first_day = datetime.date(today.year, 1, 1)
    return this_year_first_day


def get_date_fmt(date, fmt_when='%Y-%m-%d', fmt_then='%Y%m%d'):
    return datetime.datetime.strptime(str(date), fmt_when).strftime(fmt_then)


if __name__ == '__main__':
    last_month_first_day, last_month_last_day = get_last_month_first_last_day()
    print(last_month_last_day)
    print(last_month_first_day)
