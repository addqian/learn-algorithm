import threading
import time
from concurrent.futures import ThreadPoolExecutor

from autil.db_util_pool import DBPool


def task(i, table_name, length, calledid):
    last_time = time.time()

    rnt = -1

    if calledid is not None and len(calledid) > 10:
        sql = f''' update {table_name} set CalledID = '{calledid[:-4] + '****'}' where CalledID='{calledid}' '''
        rnt = DBPool.set(DBPool.connect(), sql, False)
        print(f'finish in thread:{threading.current_thread().ident}/{len(threading.enumerate())} i:{i}/{length} rnt:{rnt} - {table_name} {sql} {(time.time() - last_time):.3}s\n')
    return rnt


if __name__ == '__main__':
    last_time = time.time()
    DBPool.mssql_pool_3326('')

    table_name_list = []

    for table_name in table_name_list:
        sql = f'''  '''
        calledid_list = DBPool.get(DBPool.connect(), sql, False)
        length = len(calledid_list)
        if length == 0: continue
        calledid_list = [calledid[0] for calledid in calledid_list]
        calledid_list = list(set(calledid_list))

        with ThreadPoolExecutor() as executor:
            futures = [executor.submit(task, i, table_name, length, calledid_list[i]) for i in range(len(calledid_list))]
            results = [future.result() for future in futures]

    print(f'finish {sum(results)} {(time.time() - last_time):.3}s')
