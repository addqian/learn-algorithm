import threading
import time
from concurrent.futures import ThreadPoolExecutor

from autil.db_util_pool import DBPool


def task(i, data):
    last_time = time.time()
    CalledID = data[0]
    rnt = -1
    if CalledID is None or len((CalledID)) < 10: return rnt
    sql = f''' update RecordOriginalData set CalledID='{CalledID[:-4] + '****'}' where CalledID='{CalledID}' '''
    rnt = DBPool.set(DBPool.connect(), sql, False)
    print(f'finish in thread:{threading.current_thread().ident}/{len(threading.enumerate())} i:{i} rnt:{rnt} - {sql} {(time.time() - last_time):.3}s\n')
    return rnt


if __name__ == "__main__":
    last_time = time.time()
    DBPool.mssql_pool_3326()

    sql = '''  '''
    calledid_list = DBPool.get(DBPool.connect(), sql)
    calledid_list = [calledid[0] for calledid in calledid_list]
    calledid_list = list(set(calledid_list))

    # 1902403
    print(f'{len(calledid_list)} get use {int(time.time() - last_time)}s')

    with ThreadPoolExecutor(max_workers=64) as executor:
        # 提交任务到线程池
        futures = [executor.submit(task, i, calledid_list[i]) for i in range(len(calledid_list))]
        results = [future.result() for future in futures]

    print(f'finish {sum(results)} {(time.time() - last_time):.3}s')
