import threading

import time
from concurrent.futures import ThreadPoolExecutor


def task(a, b=1):
    last_time = time.time()
    print(f"start {a} thread:{threading.current_thread().ident}/{len(threading.enumerate())}")

    time.sleep(3)

    print(f"finish {a} {(time.time() - last_time):.3}s\n")
    return a * b


if __name__ == "__main__":
    last_time = time.time()
    with ThreadPoolExecutor(max_workers=3) as executor:
        # 提交任务到线程池
        futures = [executor.submit(task, i, i * 2) for i in range(5)]

        results = [future.result() for future in futures]

    print(f"finish {results} {(time.time() - last_time):.3}s")
