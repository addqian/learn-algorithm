import time
from concurrent.futures import ThreadPoolExecutor
from autil.db_util_pool import DBPool


def task(a, b=0):
    last_time = time.time()
    print(f"start {a}")
    time.sleep(1)
    DBPool.get(DBPool.connect(), '')
    print(f"finish {a} {int(time.time() - last_time)}s")
    return a * b


if __name__ == "__main__":
    DBPool.mssql_pool_25()
    last_time = time.time()

    with ThreadPoolExecutor(max_workers=3) as executor:
        # 提交任务到线程池
        futures = [executor.submit(task, i, i * 2) for i in range(3)]

        results = [future.result() for future in futures]

    print(f"所有任务已完成，结果为：{results} {int(time.time() - last_time)}s")
