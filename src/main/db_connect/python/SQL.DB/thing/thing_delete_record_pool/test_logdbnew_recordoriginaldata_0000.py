import threading
import time
from concurrent.futures import ThreadPoolExecutor

from autil.db_util_pool import DBPool


def task(i, table_name):
    last_time = time.time()

    rnt = -1
    sql = f'''  '''
    calledid_list = DBPool.get(DBPool.connect(), sql, False)
    calledid_list = [calledid[0] for calledid in calledid_list]
    calledid_list = list(set(calledid_list))
    if (len(calledid_list) != 0):
        for calledid in calledid_list:
            if calledid is not None and len(calledid) > 10:
                sql = f''' update {table_name} set CalledID = '{calledid[:-4] + '****'}' where CalledID='{calledid}' '''
                rnt = DBPool.set(DBPool.connect(), sql, False)
                print(f'finish in thread:{threading.current_thread().ident}/{len(threading.enumerate())} i:{i} rnt:{rnt} - {table_name} {sql} {(time.time() - last_time):.3}s\n')
    return rnt


if __name__ == '__main__':
    last_time = time.time()
    DBPool.mssql_pool_3326('')

    table_name_list = []

    with ThreadPoolExecutor() as executor:
        futures = [executor.submit(task, i, table_name_list[i]) for i in range(len(table_name_list))]
        results = [future.result() for future in futures]

    print(f'finish {sum(results)} {(time.time() - last_time):.3}s')
