import threading
import time
from concurrent.futures import ThreadPoolExecutor

from autil.db_util_pool import DBPool


def task(i, code):
    last_time = time.time()

    rnt = -1

    if code is None or len((code)) < 10: return rnt
    # sql = f''' UPDATE {table_name} SET CalledID = '{code[:-4] + '****'}' WHERE CalledID='{code}' '''
    sql = f''' UPDATE {table_name} SET {field_name} = '{code[:-4] + '****'}' WHERE {field_name}='{code}' '''
    rnt = DBPool.set(DBPool.connect(), sql, False)
    print(f'finish in thread:{threading.current_thread().ident}/{len(threading.enumerate())} i:{i} rnt:{rnt} - {sql} {(time.time() - last_time):.3}s\n ')
    return rnt


table_name = 'customeroriginaldata'  # 'RecordOriginalData'
field_name = 'callernumber'  # 'CalledID'

if __name__ == '__main__':
    last_time = time.time()
    # RecordOriginalData
    DBPool.mssql_pool_3326()

    # 1753262959566074 get use 385s
    # 18592118 get
    # 13136300
    # 12811394
    # 1902403
    # 1335573
    sql = f'''  '''

    code_list = DBPool.get(DBPool.connect(), sql)
    print(f'{len(code_list)} get use {int(time.time() - last_time)}s')

    code_list = [code[0] for code in code_list]

    code_list = list(set(code_list))
    length = len(code_list)
    print(f'{len(code_list)} get')

    with ThreadPoolExecutor(max_workers=32) as executor:
        futures = [executor.submit(task, length - 1 - i, code_list[i]) for i in range(length)]
        results = [future.result() for future in futures]

    print(f'finish {sum(results)} {(time.time() - last_time):.3}s')
