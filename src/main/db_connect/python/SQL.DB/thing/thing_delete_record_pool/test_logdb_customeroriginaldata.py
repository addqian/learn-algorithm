import threading
import time
from concurrent.futures import ThreadPoolExecutor

from autil.db_util_pool import DBPool


def task(i, data):
    last_time = time.time()
    callernumber = data[0]
    rnt = -1
    if callernumber is None or len((callernumber)) < 10: return rnt
    # print(f"start {code}")
    # time.sleep(1)
    sql = f''' update customeroriginaldata set callernumber='{callernumber[:-4] + '****'}' where callernumber='{callernumber}' '''
    # sql = f''' update customeroriginaldata set code='{code[:-4] + '****'}' where code = '{code}' '''
    rnt = DBPool.set(DBPool.connect(), sql, False)
    print(f'finish in thread:{threading.current_thread().ident}/{len(threading.enumerate())} i:{i} rnt:{rnt} - {sql} {(time.time() - last_time):.3}s\n')
    return rnt


if __name__ == "__main__":
    last_time = time.time()
    DBPool.mssql_pool_3326()

    sql = '''  '''
    code_list = DBPool.get(DBPool.connect(), sql)
    # 1336119 get use 59s

    print(f'{len(code_list)} get use {int(time.time() - last_time)}s')

    with ThreadPoolExecutor() as executor:
        # 提交任务到线程池
        futures = [executor.submit(task, i, code_list[i]) for i in range(len(code_list))]
        results = [future.result() for future in futures]

    print(f'finish {sum(results)} {(time.time() - last_time):.3}s')
