import os
from configparser import ConfigParser


class Config():
    file_path = os.path.join("../db.ini")

    cf = ConfigParser()
    cf.read(file_path)

    @staticmethod
    def read(section, option=None):
        if option is None:
            return Config.cf[section]
        return Config.cf[section][option]
