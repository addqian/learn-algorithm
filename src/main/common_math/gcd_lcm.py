"""
求最小公倍数_牛客题霸_牛客网
https://www.nowcoder.com/practice/22948c2cad484e0291350abad86136c3?tpId=37&tqId=21331&rp=1
gcd()
lcm()
"""

from line_profiler import profile


@profile
def gcd(a: int, b: int):
    """
    Intro: 求最大公约数(辗转相除法)
    Verify: ✔️
    """
    if a < b: a, b = b, a
    while b != 0:
        a, b = b, a % b

    return a


@profile
def gcd2(a: int, b: int):
    """
    Intro: 求最大公约数(更相减损法)
    Verify: ✔️
    """
    while a != b:
        if a < b: a, b = b, a
        a, b = b, a - b
    return b


def lcm(a: int, b: int):
    """
    Intro: 求最小公倍数
    Verify: ✔️
    """
    return a * b // gcd2(a, b)


if __name__ == '__main__':
    print(gcd(12, 18))
    print(gcd(2, 3))
