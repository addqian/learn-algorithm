/*
    分割字符串
    MTK联发科2021 嵌入式C笔试题分析-CSDN博客
    https://blog.csdn.net/weiqifa0/article/details/107454793
*/

#include <stdio.h>
#include <string.h>

void str_split(char *str, char split)
{
    char word[128];
    int idx = 0;
    for (int i = 0; i <= strlen(str); ++i)
    {
        if (str[i] == split || str[i] == '\0')
        {
            if (idx != 0)
            {
                word[idx] = '\0';
                printf("%s\n", word);
            }
            idx = 0;
        }
        else
        {
            word[idx++] = str[i];
        }
    }
}

int main()
{
    char str[128];
    char split;

    while (scanf("%s\n%c", str, &split) != EOF)
    {
        // printf("str=%s/%d\tsplit=%c\n", str, strlen(str), split);
        str_split(str, split);
    }
    return 0;
}
