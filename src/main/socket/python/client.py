import socket

# 创建 Socket 对象
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 设置服务器地址和端口
server_host = 'localhost'
server_port = 12345

# 连接到服务器
client_socket.connect((server_host, server_port))

# 发送消息到服务器
message_to_send = input("enter a message to send to the server: ")
client_socket.sendall(message_to_send.encode('utf-8'))

# 接收来自服务器的响应
response = client_socket.recv(1024).decode('utf-8')
print(f"response from the server: {response}")

# 关闭客户端连接
client_socket.close()