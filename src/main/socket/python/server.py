import socket

# 创建 Socket 对象
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 设置端口
server_port = 12345
server_socket.bind(('localhost', server_port))

# 设置最大连接数
server_socket.listen(3)

print(f"server is listening on port {server_port}..")

while True:
    # 接受客户端连接
    client_socket, client_address = server_socket.accept()
    print(f"accepted connection from: {client_address[0]}:{client_address[1]}")

    # 接收客户端的消息
    received_data = client_socket.recv(1024).decode('utf-8')
    print(f"received message: {received_data}")

    # 发送回复消息给客户端
    response_message = f"server response: {received_data}"
    client_socket.sendall(response_message.encode('utf-8'))

    # 关闭客户端连接
    client_socket.close()